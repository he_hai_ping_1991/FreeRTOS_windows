/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例与示例10都是为了演示不同优先级任务，通过消息队列传递消息。
		要注意的是任务优先级的不同，对队列是空或是满的影响。
   
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 将要创建的任务，两个发送任务实例，一个接收任务实例 */
static void vSenderTask( void *pvParameters );
static void vReceiverTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 声明一个QueueHandle_t 类型的变量，
用于存储三个任务使用的队列信息 */
QueueHandle_t xQueue;

typedef enum
{
	eSender1,
	eSender2
} DataSource_t;

/* 定义传送到队列的数据结构. */
typedef struct
{
	uint8_t ucValue;
	DataSource_t eDataSource;
} Data_t;

/* 声明两个Data_t类型的数据，将用于传给队列. */
static const Data_t xStructsToSend[ 2 ] =
{
	{ 100, eSender1 }, /* 用于发送者1 Sender1. */
	{ 200, eSender2 }  /* 用于发送者2 Sender2. */
};

int main( void )
{
    /* 该创建的队列将最多持有3个Data_t类型的数据 */
    xQueue = xQueueCreate( 3, sizeof( Data_t ) );

	if( xQueue != NULL )
	{
		/* 创建两个实例任务，向队列写入消息。任务参数是将写入队列的结构体类型。
		所以任务一不停的将xStructsToSend[ 0 ]发送给队列，同时任务二将不停的将
		xStructsToSend[ 1 ]发送给队列。两个发送任务的优先级都是2,它们的优先级
		都高于接收任务的优先级。 */
		xTaskCreate( vSenderTask, "Sender1", 1000, ( void * ) &( xStructsToSend[ 0 ] ), 2, NULL );
		xTaskCreate( vSenderTask, "Sender2", 1000, ( void * ) &( xStructsToSend[ 1 ] ), 2, NULL );

		/* 创建一个队钱从队列读取消息. 该任务的优先级为1,将低于两发送者任务的优先级 */
		xTaskCreate( vReceiverTask, "Receiver", 1000, NULL, 1, NULL );

		/* 开启调度，使得任务被执行. */
		vTaskStartScheduler();
	}
	else
	{
		/* 队列可能没有被创建成功. */
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vSenderTask( void *pvParameters )
{
BaseType_t xStatus;
const TickType_t xTicksToWait = pdMS_TO_TICKS( 100UL );

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 第一个参数是接收数据的队列(queue)，
		该队列是在任务调度之前创建的，所以在本任务开始之前就可以执行

		第二个参数是将要发送的数据结构的地址，这个地址是任务参数传进来的地址r.

		第三个是阻塞时间,即当队列为满的时候，阻塞当前任务一段时间，
		直到队列有空间可以写入消息.当队列满时阻塞时间将起作用，
		只有两个发送任务都处理阻塞状态时，队列中的消息才会被取走 */
		xStatus = xQueueSendToBack( xQueue, pvParameters, xTicksToWait );

		if( xStatus != pdPASS )
		{
			/* 因为队列满了，我们没有把消息写入队列-
			这肯定是出错了，因为当两例发送队列进入阻塞时，接收队列会尽可能快的
			把消息取走。（所以不会直到发送超时了，还没有把消息发送出去）*/
			vPrintString( "Could not send to the queue.\r\n" );
		}
	}
}
/*-----------------------------------------------------------*/

static void vReceiverTask( void *pvParameters )
{
/* 声明一个结构体变量接收来自队列的消息值. */
Data_t xReceivedStructure;
BaseType_t xStatus;

	/* 该任务也是一个无限循环. */
	for( ;; )
	{
		/* 由于本任务只有在两个发送任务都处于阻塞态时才能执行，并且两个发送任务
		只会在队列满时才会进入阻塞态，所以本任务运行时队列总时满的。
		3就是队列的长度 */
		if( uxQueueMessagesWaiting( xQueue ) != 3 )
		{
			vPrintString( "Queue should have been full!\r\n" );
		}

		/* 第一个参数是队列，将从该参数中获取数据.该队列先于任务调度之前创建
		因此在该任务运行之前，是其第一次运行。

		第二个参数是接收数据的存放地址,本例中该参数是一个变量的地址，该变量的址
		址空间满足接收数据所要求的结构体空间大小。
		第三个参数是阻塞时间，
		第三个参数是阻塞时间;即如果队列为空，保持该任务为阻塞状态，直到队列可用的最长等待时间。
		由于本任务只会在队列为满时才会运行，所以数据总是满的，所以阻塞时间是不必要的。*/
		xStatus = xQueueReceive( xQueue, &xReceivedStructure, 0 );

		if( xStatus == pdPASS )
		{
			/* 从队列成功接收数据，并将打印输出它的值,和它的发送者. */
			if( xReceivedStructure.eDataSource == eSender1 )
			{
				vPrintStringAndNumber( "From Sender 1 = ", xReceivedStructure.ucValue );
			}
			else
			{
				vPrintStringAndNumber( "From Sender 2 = ", xReceivedStructure.ucValue );
			}
		}
		else
		{
			/* 我们没有收到任何值.肯定是出错了，因为本任务只有当队列为满时才运行. */
			vPrintString( "Could not receive from the queue.\r\n" );
		}
	}
}






