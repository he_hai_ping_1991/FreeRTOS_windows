/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例演示了从队列集合的使用。
    
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 两个发送任务 */
void vSenderTask1( void *pvParameters );
void vSenderTask2( void *pvParameters );

/* 接收任务,接收者阻塞队列集合来接收两个发送任务的数据  */
void vReceiverTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 声明两个QueueHandle_t类型的变量. 这两个队列都会加入到同一个队列集合中. */
static QueueHandle_t xQueue1 = NULL, xQueue2 = NULL;

/* 声明一个 QueueSetHandle_t 类型的变量.  这个队列集合用于添加两个队列 */
static QueueSetHandle_t xQueueSet = NULL;

int main( void )
{
	/* 创建两个队列，每个队列发送一个字符串指针。
	接收任务的优先级高于发送任务的优先级，所以队列中任何时候保持的数据项不会超过1 */
    xQueue1 = xQueueCreate( 1, sizeof( char * ) );
	xQueue2 = xQueueCreate( 1, sizeof( char * ) );

	/* 创建队列集合. 两个队列各自包含一个数据项，所以队列集合最大要能容纳两个队列句柄 There are two queues both of which can contain
	1 item, so the maximum number of queue handle the queue set will ever have
	to hold is 2 (1 item multiplied by 2 sets). */
	xQueueSet = xQueueCreateSet( 1 * 2 );

	/* 把两个队列加到一个集合中. */
	xQueueAddToSet( xQueue1, xQueueSet );
	xQueueAddToSet( xQueue2, xQueueSet );

	/* 创建发送任务. */
	xTaskCreate( vSenderTask1, "Sender1", 1000, NULL, 1, NULL );
	xTaskCreate( vSenderTask2, "Sender2", 1000, NULL, 1, NULL );

	/* 创建接收任务. */
	xTaskCreate( vReceiverTask, "Receiver", 1000, NULL, 2, NULL );

	/* 启动调度，开始执行任务 */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vSenderTask1( void *pvParameters )
{
const TickType_t xBlockTime = pdMS_TO_TICKS( 100 );
const char * const pcMessage = "Message from vSenderTask1\r\n";

	/* 跟大多数任务一样，这个任务的实现是一个无限循环 */
	for( ;; )
	{
		/* 阻塞100ms. */
		vTaskDelay( xBlockTime );

		/* 发送任务的字符串给队列1;虽然队列只能持有一个数据项，
		但这里没有必要使用阻塞时间(队列发送的阻塞时间)，这是因为
		读取任务的优先级高于此发送任务的优先级;当这个任务写入数据
		到队列时，它将被高优先级的读取任务抢占。所以当调用xQueueSend()
		返回时，队列已经被再次置空。阻塞时间为0。*/
		xQueueSend( xQueue1, &pcMessage, 0 );
	}
}
/*-----------------------------------------------------------*/

static void vSenderTask2( void *pvParameters )
{
const TickType_t xBlockTime = pdMS_TO_TICKS( 200 );
const char * const pcMessage = "Message from vSenderTask2\r\n";

	/* 与大多任务一样，本任务是一个无限循环。 */
	for( ;; )
	{
		/* 阻塞 200ms. */
		vTaskDelay( xBlockTime );

		/* 发送任务的字符串给队列2;虽然队列只能持有一个数据项，
		但这里没有必要使用阻塞时间(队列发送的阻塞时间)，这是因为
		读取任务的优先级高于此发送任务的优先级;当这个任务写入数据
		到队列时，它将被高优先级的读取任务抢占。所以当调用xQueueSend()
		返回时，队列已经被再次置空。阻塞时间为0。*/
		xQueueSend( xQueue2, &pcMessage, 0 );
	}
}
/*-----------------------------------------------------------*/

static void vReceiverTask( void *pvParameters )
{
QueueHandle_t xQueueThatContainsData;
char *pcReceivedString;

	/* 与大多任务一样，本任务是一个无限循环。 */
	for( ;; )
	{
		/* 阻塞队列集合，直到队列集合中的某个队列包含数据。
		将xQueueSelectFromSet()函数的返回值类型QueueSetMemberHandle_t转换成
		QueueHandle_t，因为我们知道队列集合中包含的是两个QueueHandle_t类型。
		(semaphores,也可以是队列集的成员)。 */
		xQueueThatContainsData = ( QueueHandle_t ) xQueueSelectFromSet( xQueueSet, portMAX_DELAY );

		/* 由于使用了无限阻塞时间，所以xQueueSelectFromSet()只有当其中一个队列
		包含数据时才会返回，并且xQueueThatContansData的值一定是正确的。
		因此当从队列queue中读取数据时，就不需要指定阻塞时间了。因为我们知道
		此时队列肯定包含数据。阻塞时间为0 */
		xQueueReceive( xQueueThatContainsData, &pcReceivedString, 0 );

		/* 打印从队列中接收的字符串. */
		vPrintString( pcReceivedString );
	}
}

