/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		演示单次触发软定时器与自动重新加载定时器的使用。
		以及通过打印时间节拍数，以证明定时器回调函数被实时执行了
    
*/

/* FreeRTOS.org 源文件头包含 */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

/* 示例包含头文件 */
#include "supporting_functions.h"

/* 单次触发和自动重入定时器的定时周期*/
#define mainONE_SHOT_TIMER_PERIOD		( pdMS_TO_TICKS( 3333UL ) )
#define mainAUTO_RELOAD_TIMER_PERIOD	( pdMS_TO_TICKS( 500UL ) )

/*-----------------------------------------------------------*/

/*
 * 单次触发和自动重入定时器对应回调函数
 */
static void prvOneShotTimerCallback( TimerHandle_t xTimer );
static void prvAutoReloadTimerCallback( TimerHandle_t xTimer );

/*-----------------------------------------------------------*/

int main( void )
{
TimerHandle_t xAutoReloadTimer, xOneShotTimer;
BaseType_t xTimer1Started, xTimer2Started;

	/* 创建单触发软定时器，并将定时器句柄保存在xOneShotTimer中 */
	xOneShotTimer = xTimerCreate( "OneShot",					/* 软定时器的名称 - FreeRTOS 没有使用. */
								  mainONE_SHOT_TIMER_PERIOD,	/* 以tick 为单位的时间周期. */
								  pdFALSE,						/* 设置 uxAutoRealod 为 pdFALSE ，创建的就是单次触发定时器 */
								  0,							/* 这个例子不使用定时器id. */
								  prvOneShotTimerCallback );	/* 软定时器的回调函数. */

	/*  创建自动加载软定时器，并将定时器句柄存在xAutoReloadTimer中 */
	xAutoReloadTimer = xTimerCreate( "AutoReload",					/* 软定时器的名称 - FreeRTOS 没有使用. */
									 mainAUTO_RELOAD_TIMER_PERIOD,	/* 以tick 为单位的时间周期. */
									 pdTRUE,						/* 设置 uxAutoRealod 为  pdTRUE,创建的就是自动加载定时器. */
									 0,								/* 这个例子不使用定时器id. */
									 prvAutoReloadTimerCallback );	/* 软定时器的回调函数. */

	/* 检查定时器是否都创建成功. */
	if( ( xOneShotTimer != NULL ) && ( xAutoReloadTimer != NULL ) )
	{
		/* 开启软定时器，使用阻塞时间0(非阻塞时间).
		由于调度还没有开始，所以这里指定的时间将会被忽略。
		（这里开启了定时器，并不表示定时器就立即运行了。
		它是在任务开始调度后才运行的）*/
		xTimer1Started = xTimerStart( xOneShotTimer, 0 );
		xTimer2Started = xTimerStart( xAutoReloadTimer, 0 );

		/* xTimerStart()函数的使用了一个叫做(定时器命令队列)来实现的。并且当
		定时器命令队列满时，xTimerStart()将会失败。定时器服务任务将不会开始，
		直到调度器开启了.所以所有发送给命令队列的命令将会保持在队列中，直到调度
		器启动。检查两个xTimerStart()是否调用成功. */
		if( ( xTimer1Started == pdPASS ) && ( xTimer2Started == pdPASS ) )
		{
			/* 开启调度 */
			vTaskStartScheduler();
		}
	}

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void prvOneShotTimerCallback( TimerHandle_t xTimer )
{
static TickType_t xTimeNow;

	/* 获取当前的tick记数值. */
	xTimeNow = xTaskGetTickCount();

	/* 输出一个字符串，显示回调函数是什么时候被调用执行了。 */
	vPrintStringAndNumber( "One-shot timer callback executing", xTimeNow );
}
/*-----------------------------------------------------------*/

static void prvAutoReloadTimerCallback( TimerHandle_t xTimer )
{
static TickType_t xTimeNow;

	/* 获取当前的tick记数值. */
	xTimeNow = xTaskGetTickCount();

	/* 输出一个字符串，显示回调函数是什么时候被调用执行了。 */
	vPrintStringAndNumber( "Auto-reload timer callback executing", xTimeNow );
}
/*-----------------------------------------------------------*/








