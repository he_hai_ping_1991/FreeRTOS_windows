/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
	说明:
		本示例主要为了演示使用事件组标志位，用于多个任务的同步操作。
		主要是介绍xEventGroupSync函数的使用
*/

/* 标准头包含 - 用于产生随机数种子. */
#include <time.h>

/* FreeRTOS.org 源文件头包含  */
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

/* 示例包含 */
#include "supporting_functions.h"

/* 定义事件组中的事件位. */
#define mainFIRST_TASK_BIT	( 1UL << 0UL ) /* 事件位 0, 由第1个任务设置. */
#define mainSECOND_TASK_BIT	( 1UL << 1UL ) /* 事件位 1, 由第2个任务设置. */
#define mainTHIRD_TASK_BIT	( 1UL << 2UL ) /* 事件位 2, 由第3个任务设置. */

/* 伪随机数产生函数-在本文件实现，是由于MSVC rand()函数会产生意外的结果 */
static uint32_t prvRand( void );
static void prvSRand( uint32_t ulSeed );

/* 该任务创建三个实例. */
static void vSyncingTask( void *pvParameters );

/*-----------------------------------------------------------*/

/* 用于伪随机数产生. */
static uint32_t ulNextRand;

/* 声明事件组用于同步三个任务. */
EventGroupHandle_t xEventGroup;

int main( void )
{
	/* 本例中的任务阻塞时间是随机的。阻塞时间使用rand()来产生，
	这里设置随机种子值。 */
	prvSRand( ( uint32_t ) time( NULL ) );

	/* 在使用事件组之前，先要创建  */
	xEventGroup = xEventGroupCreate();

	/* 创建三个任务实例。每个任务都给一个不同的名字，
	用于后面区分那个任务执行了。同步事件位通过参数传递给任务*/
	xTaskCreate( vSyncingTask, "Task 1", 1000, ( void * ) mainFIRST_TASK_BIT, 1, NULL );
	xTaskCreate( vSyncingTask, "Task 2", 1000, ( void * ) mainSECOND_TASK_BIT, 1, NULL );
	xTaskCreate( vSyncingTask, "Task 3", 1000, ( void * ) mainTHIRD_TASK_BIT, 1, NULL );

	/* 开启调度，执行任务 */
	vTaskStartScheduler();

	/* 下面的代码应该是永远不会被执行的，除非vTaskStartScheduler() 函数没有足够的堆(heap)
	空间创建空闲任务和定时器(如果配置了定时器)任务。关于堆管理和捕获堆空间耗尽的相关技术，
	在书本正文中有描述。这里的堆就是我们常说的内存，大多数情况下是指高速访问内存RAM. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

static void vSyncingTask( void *pvParameters )
{
const EventBits_t uxAllSyncBits = ( mainFIRST_TASK_BIT | mainSECOND_TASK_BIT | mainTHIRD_TASK_BIT );
const TickType_t xMaxDelay = pdMS_TO_TICKS( 4000UL );
const TickType_t xMinDelay = pdMS_TO_TICKS( 200UL );
TickType_t xDelayTime;
EventBits_t uxThisTasksSyncBit;

	/* 该任务创建了三个实例--每个任务实例使用不同的事件位进行同步。
	该作务的事件同步位是通过过任务参数传递进来的，将其存储在变量
	uxThisTasksSyncBit中。 */
	uxThisTasksSyncBit = ( EventBits_t ) pvParameters;

	for( ;; )
	{
		/* 通过延迟一段随机时间，来模拟任务花费一段时间执行某个动作.
		这样使得三个实例不会在同一时间，到达同步执行点，并使更容易
		的观察得任务的行为 */
		xDelayTime = ( prvRand() % xMaxDelay ) + xMinDelay;
		vTaskDelay( xDelayTime );

		/* 打印一个消息，表示任务已经到达了它的同步点。
		pcTaskGetTaskName 函数可以获得任务创建时的名称。 */
		vPrintTwoStrings( pcTaskGetTaskName( NULL ), "reached sync point" );

		/* 等待所有任务都到达了相应(各自)的同步点 */
		xEventGroupSync( /* 用于同步的事件组 */
						 xEventGroup,

						 /* 设置任务的同步位，表示该任务到达了同步点. */
						 uxThisTasksSyncBit,

						 /* 要等待的同步位，每一位代表一个参与同步任务*/
						 uxAllSyncBits,

						 /* 一直等待，直到所有任务都到达同步点 */
						 portMAX_DELAY );

		/* 输出一个消息，表示该任务已经经过了同步点。
		由于上面使用的是无限延迟时间，所以下面的代码，只有当所有的任务
		都到达了他们的相应的同步点，才会被执行.*/
		vPrintTwoStrings( pcTaskGetTaskName( NULL ), "exited sync point" );
	}
}
/*-----------------------------------------------------------*/

static uint32_t prvRand( void )
{
const uint32_t ulMultiplier = 0x015a4e35UL, ulIncrement = 1UL;
uint32_t ulReturn;

	/* 由于MSVC rand()函数会产生不可预知的结果,
	所以使用此工具函数用于产生伪随机数。 */
	taskENTER_CRITICAL();
		ulNextRand = ( ulMultiplier * ulNextRand ) + ulIncrement;
		ulReturn = ( ulNextRand >> 16UL ) & 0x7fffUL;
	taskEXIT_CRITICAL();
	return ulReturn;
}
/*-----------------------------------------------------------*/

static void prvSRand( uint32_t ulSeed )
{
	/* 工具函数，用于产生伪随机数种子 */
	ulNextRand = ulSeed;
}
/*-----------------------------------------------------------*/




