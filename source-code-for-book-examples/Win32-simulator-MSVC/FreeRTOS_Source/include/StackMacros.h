/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    
	http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。
*/

#ifndef STACK_MACROS_H
#define STACK_MACROS_H

/*
 * 如果正在交换的任务的堆栈当前溢出，或者看起来像是在过去溢出过，
 * 则调用stack overflow hook函数。
 * 
 * 设置configCHECK_FOR_STACK_OVERFLOW的值为1,将会使得宏检查当前任务的状态，
 * 即比较栈顶值与栈的最大限制值。
 * 将configCHECK_FOR_STACK_OVERFLOW设置为大于1还将导致检查最后几个堆栈字节，
 * 以确保创建任务时设置的字节值没有被覆盖。
 * 注：第二个测试并不保证总是能识别溢出的栈。
 */

/*-----------------------------------------------------------*/

#if( ( configCHECK_FOR_STACK_OVERFLOW == 1 ) && ( portSTACK_GROWTH < 0 ) )

	/* 只检测当前任务的栈的状态 */
	#define taskCHECK_FOR_STACK_OVERFLOW()																\
	{																									\
		/* 当前保存的栈指针是否在栈限制内？ */								\
		if( pxCurrentTCB->pxTopOfStack <= pxCurrentTCB->pxStack )										\
		{																								\
			vApplicationStackOverflowHook( ( TaskHandle_t ) pxCurrentTCB, pxCurrentTCB->pcTaskName );	\
		}																								\
	}

#endif /* configCHECK_FOR_STACK_OVERFLOW == 1 */
/*-----------------------------------------------------------*/

#if( ( configCHECK_FOR_STACK_OVERFLOW == 1 ) && ( portSTACK_GROWTH > 0 ) )

	/* 只检测当前任务的栈的状态. */
	#define taskCHECK_FOR_STACK_OVERFLOW()																\
	{																									\
																										\
		/* 当前保存的栈指针是否在栈限制内？ */								\
		if( pxCurrentTCB->pxTopOfStack >= pxCurrentTCB->pxEndOfStack )									\
		{																								\
			vApplicationStackOverflowHook( ( TaskHandle_t ) pxCurrentTCB, pxCurrentTCB->pcTaskName );	\
		}																								\
	}

#endif /* configCHECK_FOR_STACK_OVERFLOW == 1 */
/*-----------------------------------------------------------*/

#if( ( configCHECK_FOR_STACK_OVERFLOW > 1 ) && ( portSTACK_GROWTH < 0 ) )

	#define taskCHECK_FOR_STACK_OVERFLOW()																\
	{																									\
		const uint32_t * const pulStack = ( uint32_t * ) pxCurrentTCB->pxStack;							\
		const uint32_t ulCheckValue = ( uint32_t ) 0xa5a5a5a5;											\
																										\
		if( ( pulStack[ 0 ] != ulCheckValue ) ||												\
			( pulStack[ 1 ] != ulCheckValue ) ||												\
			( pulStack[ 2 ] != ulCheckValue ) ||												\
			( pulStack[ 3 ] != ulCheckValue ) )												\
		{																								\
			vApplicationStackOverflowHook( ( TaskHandle_t ) pxCurrentTCB, pxCurrentTCB->pcTaskName );	\
		}																								\
	}

#endif /* #if( configCHECK_FOR_STACK_OVERFLOW > 1 ) */
/*-----------------------------------------------------------*/

#if( ( configCHECK_FOR_STACK_OVERFLOW > 1 ) && ( portSTACK_GROWTH > 0 ) )

	#define taskCHECK_FOR_STACK_OVERFLOW()																								\
	{																																	\
	int8_t *pcEndOfStack = ( int8_t * ) pxCurrentTCB->pxEndOfStack;																		\
	static const uint8_t ucExpectedStackBytes[] = {	tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE,		\
													tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE,		\
													tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE,		\
													tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE,		\
													tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE, tskSTACK_FILL_BYTE };	\
																																		\
																																		\
		pcEndOfStack -= sizeof( ucExpectedStackBytes );																					\
																																		\
		/* 任务堆栈的末端是否被重写过？ */																\
		if( memcmp( ( void * ) pcEndOfStack, ( void * ) ucExpectedStackBytes, sizeof( ucExpectedStackBytes ) ) != 0 )					\
		{																																\
			vApplicationStackOverflowHook( ( TaskHandle_t ) pxCurrentTCB, pxCurrentTCB->pcTaskName );									\
		}																																\
	}

#endif /* #if( configCHECK_FOR_STACK_OVERFLOW > 1 ) */
/*-----------------------------------------------------------*/

/* 删除堆栈溢出宏（如果未使用）. */
#ifndef taskCHECK_FOR_STACK_OVERFLOW
	#define taskCHECK_FOR_STACK_OVERFLOW()
#endif



#endif /* STACK_MACROS_H */

