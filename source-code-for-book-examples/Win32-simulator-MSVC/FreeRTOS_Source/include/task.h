/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
    
	http://www.FreeRTOS.org/FAQHelp.html - 有问题吗? 阅读FAQ页面
    "我的应用不能运行, 有可能哪里出氏了?". 你定义了configASSERT()吗?

    http://www.FreeRTOS.org/support - 作为收到如此高质量软年的回馈，我们邀请你
    通过参与支持论坛，来支持我们的全球社区

    http://www.FreeRTOS.org/training - 投资于培训可以让你的团队尽可能早地提高生产力。
    现在，您可以直接从Real Time Engineers Ltd首席执行官Richard Barry
    那里获得FreeRTOS培训，他是全球领先RTO的权威机构。
    
    http://www.FreeRTOS.org/plus - 一系列FreeRTOS生态系统产品，
    包括FreeRTOS+Trace——一个不可或缺的生产力工具，一个与DOS兼容的FAT文件系统，
    以及我们的微型线程感知UDP/IP协议栈。

    http://www.FreeRTOS.org/labs - FreeRTOS新产品的孵化地。快来试试FreeRTOS+TCP，
    我们为FreeRTOS开发的新的开源TCP/IP协议栈。

    http://www.OpenRTOS.com - Real Time Engineers ltd.向High Integrity Systems ltd.
    授权FreeRTOS以OpenRTOS品牌销售。低成本的OpenRTOS许可证提供有票证的支持、赔偿和商业中间件。

    http://www.SafeRTOS.com - 高完整性系统还提供安全工程和独立SIL3认证版本，
    用于需要可证明可靠性的安全和关键任务应用。
*/


#ifndef INC_TASK_H
#define INC_TASK_H

#ifndef INC_FREERTOS_H
	#error "include FreeRTOS.h must appear in source files before include task.h"
#endif

#include "list.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------
 * MACROS AND DEFINITIONS
 *----------------------------------------------------------*/

#define tskKERNEL_VERSION_NUMBER "V9.0.0"
#define tskKERNEL_VERSION_MAJOR 9
#define tskKERNEL_VERSION_MINOR 0
#define tskKERNEL_VERSION_BUILD 0

/**
 * task. h
 *
 * Type 指向某个任务的类型。比如：xTaskCreate调用返回一个
 * TaskHandle_t 类型的变量(通过指针)，这个变量可以在调用vTaskDelete时
 * 做为一个参数，用于删除任务
 */
typedef void * TaskHandle_t;

/*
 * 定义应用钩子函数必须要尊寻的函数原型
 */
typedef BaseType_t (*TaskHookFunction_t)( void * );

/* eTaskGetState函数返回的任务状态类型信息. */
typedef enum
{
	eRunning = 0,	/* 一个查正在查询自己的状态的任务，所以肯定是正在运行 */
	eReady,			/* 被查询的任务是处于就绪或等待(pending)就绪列表中 */
	eBlocked,		/* 被查询的任务处于阻塞状态 */
	eSuspended,		/* 被查询的任务处于挂起(Suspended)状态，或者处于无限等待阻塞(Blocked)状态 */
	eDeleted,		/* 被查询的任务已经被删除,但它的TCB控制块还没有释放. */
	eInvalid		/* 表示非法状态值(无效的) */
} eTaskState;

/* 当调用vTaskNotify()后，要执行的动作. */
typedef enum
{
	eNoAction = 0,				/* 通知任务而不更新其Notify值. */
	eSetBits,					/* 设置任务的通知值中的设置位 */
	eIncrement,					/* 增加任务的通知值. */
	eSetValueWithOverwrite,		/* 将任务的通知值设置为特定值，即使任务尚未读取以前的值. */
	eSetValueWithoutOverwrite	/* 如果任务已读取上一个值，则设置任务的通知值. */
} eNotifyAction;

/*
 * 仅内部使用.
 */
typedef struct xTIME_OUT
{
	BaseType_t xOverflowCount;
	TickType_t xTimeOnEntering;
} TimeOut_t;

/*
 * 当使用MPU功能时，定义可以分配给任务的内存范围
 */
typedef struct xMEMORY_REGION
{
	void *pvBaseAddress;
	uint32_t ulLengthInBytes;
	uint32_t ulParameters;
} MemoryRegion_t;

/*
 *用于创建MPU受保护任务的参数
 */
typedef struct xTASK_PARAMETERS
{
	TaskFunction_t pvTaskCode;
	const char * const pcName;	/*在编码合规检测!e971这一条，允许使用非限定字符类型，并且只允许single字符串类型*/
	uint16_t usStackDepth;
	void *pvParameters;
	UBaseType_t uxPriority;
	StackType_t *puxStackBuffer;
	MemoryRegion_t xRegions[ portNUM_CONFIGURABLE_REGIONS ];
} TaskParameters_t;

/* 
 * 用于uxTaskGetSystemState()函数中，
 * 返回系统中每个任务的状态
 */
typedef struct xTASK_STATUS
{
	TaskHandle_t xHandle;			/* 与结构中的其余信息相关联的任务的句柄. */
	const char *pcTaskName;			/* 指向任务名称的指针。如果在填充结构后删除了任务，则此值将无效! */ /*在编码合规检测!e971这一条，允许使用非限定字符类型，并且只允许single字符串类型*/
	UBaseType_t xTaskNumber;		/* 任务的唯一id号. */
	eTaskState eCurrentState;		/* 填充结构时任务存在的状态. */
	UBaseType_t uxCurrentPriority;	/* 填充结构时任务正在运行（也许被继承了）的优先级 */
	UBaseType_t uxBasePriority;		/* 如果任务的当前优先级已被继承以避免获取互斥锁时无限制的优先级反转，则任务将返回的优先级。只有当 configUSE_MUTEXES 在 FreeRTOSConfig.h定义为 1 时才有效. */
	uint32_t ulRunTimeCounter;		/* 到目前为止分配给任务的总运行时间，由运行时统计时钟定义. 只有当 configGENERATE_RUN_TIME_STATS 的值在 FreeRTOSConfig.h定义为 1 时才有效。详情: http://www.freertos.org/rtos-run-time-stats.html.*/
	StackType_t *pxStackBase;		/* 指向任务堆栈区域的最低地址。 */
	uint16_t usStackHighWaterMark;	/* 自任务创建以来，该任务剩余的最小堆栈空间量。此值越接近于零，则任务越接近栈溢出。 */
} TaskStatus_t;

/* eTaskConfirmSleepModeStatus（）返回的所有可能状态值枚举. */
typedef enum
{
	eAbortSleep = 0,		/* 自从调用portSuppless_TICKS_AND_SLEEP（）后，任务已准备就绪或上下文开关挂起-中止进入睡眠模式. */
	eStandardSleep,			/* 进入睡眠模式，其持续时间不会超过预期的空闲时间. */
	eNoTasksWaitingTimeout	/* 没有任务在等待超时，因此可以安全地进入睡眠模式，该模式只能通过外部中断退出. */
} eSleepModeStatus;

/**
 * idle 任务的优先级.  不要修改此值.
 *
 * \ingroup TaskUtils
 */
#define tskIDLE_PRIORITY			( ( UBaseType_t ) 0U )

/**
 * task. h
 *
 * 强制上下文切换的宏
 */
#define taskYIELD()					portYIELD()

/**
 * task. h
 *
 * 进入代码临界区的宏定义，
 * 在临界面代码执行时，不会发生抢占式上下文切
 * 注意：
 *  这里可能会修改栈(取决于具体的移植实现)所以使用时要非常小心
 */
#define taskENTER_CRITICAL()		portENTER_CRITICAL()
#define taskENTER_CRITICAL_FROM_ISR() portSET_INTERRUPT_MASK_FROM_ISR()

/**
 * task. h
 *
 * 退出代码临界区的宏定义，
 * 在临界面代码执行时，不会发生抢占式上下文切
 * 注意：
 *  这里可能会修改栈(取决于具体的移植实现)所以使用时要非常小心
 *
 */
#define taskEXIT_CRITICAL()			portEXIT_CRITICAL()
#define taskEXIT_CRITICAL_FROM_ISR( x ) portCLEAR_INTERRUPT_MASK_FROM_ISR( x )
/**
 * task. h
 *
 * 禁止所有可屏蔽中断宏
 *
 */
#define taskDISABLE_INTERRUPTS()	portDISABLE_INTERRUPTS()

/**
 * task. h
 *
 * 允许所有可屏蔽中断宏
 *
 */
#define taskENABLE_INTERRUPTS()		portENABLE_INTERRUPTS()

/* 
 * 当configASSERT（）被定义为assert（）
 * 语句中使用的常量时，taskSCHEDULER_SUSPENDED为0
 * 可以生成更优化的代码。
 */
#define taskSCHEDULER_SUSPENDED		( ( BaseType_t ) 0 )
#define taskSCHEDULER_NOT_STARTED	( ( BaseType_t ) 1 )
#define taskSCHEDULER_RUNNING		( ( BaseType_t ) 2 )


/*-----------------------------------------------------------
 * 任务创建API
 *----------------------------------------------------------*/

/**
 * task. h
 BaseType_t xTaskCreate(
							  TaskFunction_t pvTaskCode,
							  const char * const pcName,
							  uint16_t usStackDepth,
							  void *pvParameters,
							  UBaseType_t uxPriority,
							  TaskHandle_t *pvCreatedTask
						  );
 *
 * 创建一个新任务，并加入到任务列表准备运行.
 * 在FreeRTOS实现中，内部使用了两块内存。
 * 第一块内存用于保存任务的数据结构，
 * 第二个块被任务用作其堆栈。
 * 如果使用xTaskCreate()创建任务，这两块内存会自动在内部动态分配.
 * 在http://www.freertos.org/a00111.html中有说明
 * 如果使用xTaskCreateStatic()来创建任务，应用实现者需要提供必要的内存
 * 所以我们在使用xTaskCreateStatic()时，可以不用动态内存分配
 *
 * 查看 xTaskCreateStatic() 了解不需要动态内存相关信息
 * 
 * xTaskCreate()只能用于创建可无限制访问mcu所有内存空间的任务
 * 系统包含一个xTaskCreateRestricted()函数，它支持MPU,可用于创建
 * MPU 限制的任务.
 *  译注:MPU memory protected unit
 *
 * @param pvTaskCode 指向任务入口的指针. 任务的实现为永不返回.(比如一直循环)
 *
 * @param pcName 一个描述任务的名字. 主要用于方便调试
 * configMAX_TASK_NAME_LEN定义了最大长度 - 默认是16
 *
 * @param usStackDepth 任务栈的大小，表示栈可以持有多少个变量-不是多少字节
 * 比如，如果栈是32-bit宽度，如果ulStackDepth设为100，那么就需要400字节分配
 * 给栈空间
 *
 * @param pvParameters 将用作正在创建的任务的参数的指针。
 *
 * @param uxPriority 任务将运行的优先级
 * 包含MPU支持的系统，可通过设置priority参数的portPRIVILEGE_BIT
 * 特权位，创建运行于特权模式的任务。比如创建运行于特权模式优先级为2的任务。
 * 就设置uxPriority为( 2 | portPRIVILEGE_BIT ) 
 *
 * @param pvCreatedTask 用于传回句柄，通过该句柄可以引用已创建的任务。
 *
 * @return 如果任务已成功创建并添加到就绪列表中返回pdPASS，
 * 否则返回在projdefs.h文件中定义错误码
 *
 * 使用举例:
 // 任务.
 void vTaskCode( void * pvParameters )
 {
	 for( ;; )
	 {
		 // 这里写任务代码.
	 }
 }

 // 创建任务的函数
 void vOtherFunction( void )
 {
 static uint8_t ucParameterToPass;
 TaskHandle_t xHandle = NULL;

	 // 创建任务，存储句柄. 注意参数ucParameterToPass 必须在任务的整个生命周期都有效
	 // 所以这里声明为static,如果它是一个自动分配的栈变量，当任务访访问它时，
	 // 它可能已不存在，或者是已经崩溃的变量。
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, &ucParameterToPass, tskIDLE_PRIORITY, &xHandle );
     configASSERT( xHandle );

	 // 使用句柄删除该任务
     if( xHandle != NULL )
     {
	     vTaskDelete( xHandle );
     }
 }
 */
#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
	BaseType_t xTaskCreate(	TaskFunction_t pxTaskCode,
							const char * const pcName,
							const uint16_t usStackDepth,
							void * const pvParameters,
							UBaseType_t uxPriority,
							TaskHandle_t * const pxCreatedTask ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
#endif

/**
 * task. h
 TaskHandle_t xTaskCreateStatic( TaskFunction_t pvTaskCode,
								 const char * const pcName,
								 uint32_t ulStackDepth,
								 void *pvParameters,
								 UBaseType_t uxPriority,
								 StackType_t *pxStackBuffer,
								 StaticTask_t *pxTaskBuffer );
 *
 * 创建一个新任务，并加入到任务列表准备运行.
 * 在FreeRTOS实现中，内部使用了两块内存。
 * 第一块内存用于保存任务的数据结构，
 * 第二个块被任务用作其堆栈。
 * 如果使用xTaskCreate()创建任务，这两块内存会自动在内部动态分配.
 * 在http://www.freertos.org/a00111.html中有说明
 * 如果使用xTaskCreateStatic()来创建任务，应用实现者需要提供必要的内存
 * 所以我们在使用xTaskCreateStatic()时，可以不用动态内存分配
 *
 * @param pvTaskCode 指向任务入口的指针. 任务的实现为永不返回.(比如一直循环)
 * @param pcName 一个描述任务的名字. 主要用于方便调试
 * 在FreeRTOSConfig.h中configMAX_TASK_NAME_LEN定义了最大长度
 * @param ulStackDepth 任务栈的大小，表示栈可以持有多少个变量-不是多少字节
 * 比如，如果栈是32-bit宽度，如果ulStackDepth设为100，那么就需要400字节分配
 * 给栈空间
 * @param pvParameters 将用作正在创建的任务的参数的指针。
 * @param uxPriority 任务运行的优先级.
 * @param pxStackBuffer 指向一个StackType_t类型的数组，该数组至少有ulStackDepth
 *  个索引--这个数组将会用作于任务的栈，无需为栈自动分配内存
 * @param pxTaskBuffer 必须指向StaticTask_t类型的变量，该变量用于任务的数据结构
 * 无需为该变量自动分配内存
 * @return 如果pxStackBuffer或者pxTaskBuffer都不为空，那么任务将成功创建，并返回
 * pdPASS, 如果pxStackBuffer或者pxTaskBuffer为空，那么将不会创建任务，并返回
 * errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY (可能内存不足)
 *
 * 使用举例:
    // 指定被创建任务的栈的buffer大小
    // 注意:  这个数值是指栈有多少个word,而不是多少个字节
    // 例如, 如果栈的每个项是32-bit宽度，那么这里设为100,
    // 表示分配400(100 * 32-bits)字节.
    #define STACK_SIZE 200

    // 持有被创建任务的TCB块的结构
    StaticTask_t xTaskBuffer;

    // 将用于被创建任务的栈内存.
    // 注意:它是StackType_t类型的变量数组，StackType_t类型所占的
    // 字节数，由移植时决定
    StackType_t xStack[ STACK_SIZE ];

    // 任务函数.
    void vTaskCode( void * pvParameters )
    { 
        // pvParameters 应该是1,因为调用xTaskCreateStatic()传递的是1 
        configASSERT( ( uint32_t ) pvParameters == 1UL );

        for( ;; )
        {
            // 这里写任务代码.
        }
    }

    // 创建任务的函数
    void vOtherFunction( void )
    {
        TaskHandle_t xHandle = NULL;

        // 不使用动态内存分配创建任务
        xHandle = xTaskCreateStatic(
                      vTaskCode,       // 任务实现函数.
                      "NAME",          // 任务名称.
                      STACK_SIZE,      // 以words为单位的栈空间，不是以byte为单位
                      ( void * ) 1,    // 传递给任务的参数.
                      tskIDLE_PRIORITY,// 任务的优先级.
                      xStack,          // 任务栈数组.
                      &xTaskBuffer );  // 持有任务数据结构的变量.

        // puxStackBuffer 和 pxTaskBuffer 都不为空,所以任务将会被创建
        // 关且xHandle 将会是任务句柄.用它来挂起任务
        vTaskSuspend( xHandle );
    }
 */
#if( configSUPPORT_STATIC_ALLOCATION == 1 )
	TaskHandle_t xTaskCreateStatic(	TaskFunction_t pxTaskCode,
									const char * const pcName,
									const uint32_t ulStackDepth,
									void * const pvParameters,
									UBaseType_t uxPriority,
									StackType_t * const puxStackBuffer,
									StaticTask_t * const pxTaskBuffer ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
#endif /* configSUPPORT_STATIC_ALLOCATION */

/**
 * task. h
 *<pre>
 BaseType_t xTaskCreateRestricted( TaskParameters_t *pxTaskDefinition, TaskHandle_t *pxCreatedTask );</pre>
 *
 * xTaskCreateRestricted() 应只用于包含MPU的系统中
 * 创建新任务并将其添加到准备运行的任务列表中。
 * 函数参数定义内存区域和分配给任务的相关访问权限。
 *
 * @param pxTaskDefinition 指向一个结构体，
 * 该结构包含正常的xTaskCreate函数创建时的参数，
 * 外加一个可选的栈内存和指定范围的内存
 *
 * @param pxCreatedTask 用于传回句柄，通过该句柄可以引用已创建的任务
 *
 * @return pdPASS 如果任务已成功创建并添加到就绪列表中返回pdPASS，
 * 否则返回在projdefs.h文件中定义错误码
 *
 * 使用示例:
// 创建一个定义了要创建的任务的TaskParameters结构。
static const TaskParameters_t xCheckTaskParameters =
{
	vATask,		// 任务函数
	"ATask",	// 任务名称
	100,		//以WORDS为单位的栈深度
	NULL,		// 传递给任务函数的参数.
	( 1UL | portPRIVILEGE_BIT ),// 任务优先级,如果运行在特权模式还要设置特权位portPRIVILEGE_BIT.
	cStackBuffer,// 任务栈空间.

	// xRegions - 分配三个独立的内存区域给任务访问，并分别指定了相应的访问权限。
	// 不同的处理器有不同的内存对齐要求-详细信息参考FreeRTOS文档
	{
		// 基地址							长度  	参数
        { cReadWriteArray,				32,		portMPU_REGION_READ_WRITE },
        { cReadOnlyArray,				32,		portMPU_REGION_READ_ONLY },
        { cPrivilegedOnlyAccessArray,	128,	portMPU_REGION_PRIVILEGED_READ_WRITE }
	}
};

int main( void )
{
TaskHandle_t xHandle;

	// 根据上面定义的常量结构体创建任务. 任务句柄是必要的(第二个参数不为NULl)，
	// 但是这里只是为了演示，实际上没有使用
	xTaskCreateRestricted( &xRegTest1Parameters, &xHandle );

	// 启动调度
	vTaskStartScheduler();

	// 只有当没有足够内存创建idle和(或者)timer任务时，才会运行到这里
	for( ;; );
}
 */
#if( portUSING_MPU_WRAPPERS == 1 )
	BaseType_t xTaskCreateRestricted( const TaskParameters_t * const pxTaskDefinition, TaskHandle_t *pxCreatedTask ) PRIVILEGED_FUNCTION;
#endif

/**
 * task. h
 * void vTaskAllocateMPURegions( TaskHandle_t xTask, const MemoryRegion_t * const pxRegions );
 * 
 * 当通过xTaskCreateRestricted()创建一个受限的任务时，内存区域已经指定给了任务。
 * 但是这些内存区域还可以使用vTaskAllocateMPURegions()重新指定。
 *
 * @param xTask 正要更新的任务的句柄.
 * @param xRegions 一个指向MemoryRegion_t结构体类型的指针，它包含了新的内存区域定义
 *
 * 使用举例:
// 定义了一个MemoryRegion_t类型的结构体数组，它包含了一个可以read/write访问的
// 1024字节长的，基地址为ucOneKByte的MPU区域。另外两个区域不使用，所以设为0
static const MemoryRegion_t xAltRegions[ portNUM_CONFIGURABLE_REGIONS ] =
{
	// 基地址				长度			参数
	{ ucOneKByte,		1024,		portMPU_REGION_READ_WRITE },
	{ 0,				0,			0 },
	{ 0,				0,			0 }
};

void vATask( void *pvParameters )
{
    // 本任务已经创建了，并且也访问了由MPU配置指定的某些内存区域。
    // 在某个时间点上，它期望这些MPU区域由上面定义的xAltRegions常量结构值
    // 替换掉。这时就可以用vTaskAllocateMPURegions()来实现这一目的.
    // NULL 当作任务句柄参数，表示修改调用该函数(vTaskAllocateMPURegions)
    // 的任务的MPU区域。
 
	vTaskAllocateMPURegions( NULL, xAltRegions );

	// 现在任务继续它的功能，但是从这一点(指的是上面的调用函数之后)起，
	// 它只能访问它的栈和ucOneKByte数组区域
	// (除非其他地方已声明任何其他静态定义或共享区域).
}
 */
void vTaskAllocateMPURegions( TaskHandle_t xTask, const MemoryRegion_t * const pxRegions ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * void vTaskDelete( TaskHandle_t xTask );
 *
 * 要使用该函数INCLUDE_vTaskDelete必须定义为1.具体去看配置相关的信息
 * 从RTOS实时内核管理中移除一个任务。任务将会从所有就绪，阻塞，挂起和事件列表
 * 中移除。
 *
 * 注意: 空闲任务负责释放内核分配给已删除任务的内存。
 * 因此如果在应用中调用了vTaskDelete ()函数，那么空闲任务不缺少CPU处理时间
 * 就很重要(译注：指的是空闲任务需要能得到时间运行，可以释放内核分配的给已删除任务的内存)。
 * 任务代码分配的内存不会自动释放，应该在任务删除之前释放掉
 *
 * @param xTask 被删除任务的句柄.  NULL 删除调用该函数的任务
 *
 * 使用示例:
 void vOtherFunction( void )
 {
 TaskHandle_t xHandle;

	 // 创建任务，存储句柄
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, &xHandle );

	 // 使用句柄删除任务
	 vTaskDelete( xHandle );
 }
 */
void vTaskDelete( TaskHandle_t xTaskToDelete ) PRIVILEGED_FUNCTION;

/*-----------------------------------------------------------
 * 任务控制API
 *----------------------------------------------------------*/

/**
 * task. h
 * void vTaskDelay( const TickType_t xTicksToDelay );
 * 延迟一个任务，为给定的时钟节拍个数。实际阻塞时间由时钟节拍频决定
 * 宏portTICK_PERIOD_MS可以将时钟节拍转换成实际时间-- 分辨率为一个周期
 * 译注(分辨率:最小单位为一个tick周期)
 * 要使用该函数，INCLUDE_vTaskDelay必须定义为1.
 *
 * vTaskDelay() 指定相对于vTaskDelay()被调用后的某段时间，任务解除阻塞。
 * 比如，指定阻塞100tick,将会导致任务在vTaskDelay()调用后100ticks，任务
 * 解除阻塞。因此，vTaskDelay（）不能提供一个很好的方法来控制周期性任务的频率，
 * 因为通过代码的路径以及其他任务和中断活动将影响vTaskDelay（）的调用频率，
 * 从而影响任务下一次执行的时间。
 *
 * 请参见vTaskDelayUntil（），以获取旨在促进固定频率执行的替代API函数。
 * 它通过指定调用任务应该取消阻塞的绝对时间（而不是相对时间）来实现这一点
 *
 * @param xTicksToDelay 调用任务阻塞的时间值，以 tick周期为单位
 *
 * 使用举例:
 void vTaskFunction( void * pvParameters )
 {
 // 阻塞500ms.
 const TickType_t xDelay = 500 / portTICK_PERIOD_MS;

	 for( ;; )
	 {
		 // 每500ms毫秒开关一次led灯.
		 vToggleLED();
		 vTaskDelay( xDelay );
	 }
 }
 */
void vTaskDelay( const TickType_t xTicksToDelay ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>void vTaskDelayUntil( TickType_t *pxPreviousWakeTime, const TickType_t xTimeIncrement );</pre>
 *
 * INCLUDE_vTaskDelayUntil 必须为1，该函数才可用.具体参照相关配置
 *
 * 延迟任务一个指定的时间. 该函数可用于周期性任务，可保证一个恒定的运行频率
 *
 * 该函数与vTaskDelay函数之间一个重要的区别：vTaskDelay将导至任务从调用vTaskDelay函数起，
 * 阻塞一段时间.因止使用vTaskDelay很难保证一个固定的执行频率，因为从一个任务开始执行
 * 到任务调用vTaskDelay函数可能是不固定的[在两次调用之间代码的执行路径可能不同，或者
 * 在两次执行期间产生了不同的中断或不同的任务抢占次数]
 *
 * vTaskDelay指定的唤醒时间是相对于该函数被调用时间的。vTaskDelayUntil指定的是
 * 一个绝对唤醒时间
 *
 * portTICK_PERIOD_MS 可以将一个真实时间转换成tick 频率--分辨率为tick周期
 *
 * @param pxPreviousWakeTime 指向一个变量，该变保存着上次该任务退出阻塞的时间，
 * 该变量在使用前，必须首先初始化(看下面的例子)，接下来该变量将由vTaskDelayUntil
 * 自动更新
 *
 * @param xTimeIncrement 时钟周期. 任务将会在
 * time *pxPreviousWakeTime + xTimeIncrement 时解除阻塞. 
 * 调用vTaskDelayUntil给定一个相同的xTimeIncrement 值，
 * 将会使任务以固定的频率执行。
 *
 * 使用示例:
 // 每10 ticks执行一个动作.
 void vTaskFunction( void * pvParameters )
 {
 TickType_t xLastWakeTime;
 const TickType_t xFrequency = 10;

	 // 以当前时间初始化上次唤醒时间 xLastWakeTime 
	 xLastWakeTime = xTaskGetTickCount ();
	 for( ;; )
	 {
		 // 等待下一个周期
		 vTaskDelayUntil( &xLastWakeTime, xFrequency );

		 // 在这里执行动作
	 }
 }
 */
void vTaskDelayUntil( TickType_t * const pxPreviousWakeTime, const TickType_t xTimeIncrement ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>BaseType_t xTaskAbortDelay( TaskHandle_t xTask );</pre>
 *
 * INCLUDE_xTaskAbortDelay 在FreeRTOSConfig.h定义为1,该函数才能使用
 * 
 * 当任务等待一个事件时，将进入阻塞状态。等待的事件可以是一个临时事件(等待一个时间)
 * 比如调用vTaskDelay，或者一个对象事件，比如调用xQueueReceive或ulTaskNotifyTake.
 * 如果一个处于阻塞状态的任务，它的句柄传给xTaskAbortDelay()调用，将使该任务退出
 * 阻塞状态，并使该任务返回进入阻塞状态的调用处。
 *
 * @param xTask 解除阻塞状态的任务句柄.
 *
 * @return 如果被引用的任务没有处于阻塞状态那么返回pdFAIL
 * 否则返回 pdPASS
 */
BaseType_t xTaskAbortDelay( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * UBaseType_t uxTaskPriorityGet( TaskHandle_t xTask );
 * INCLUDE_uxTaskPriorityGet必须定义为1，该函数才可以用。具体请看配置
 *
 * 获取任意任务的优先级
 *
 * @param xTask 要查询任务的句柄. NULL表示获取调用任务的优先级
 *
 * @return 任务优先级
 *
 * 使用示例:
 void vAFunction( void )
 {
 TaskHandle_t xHandle;

	 // 创建任务存储句柄.
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, &xHandle );

	 // ...

	 // 使用句柄获取已创建任务的优先级.
	 // 任务创建时使用的tskIDLE_PRIORITY优先级，但有可能它自己改变了优先级
	 if( uxTaskPriorityGet( xHandle ) != tskIDLE_PRIORITY )
	 {
		 // 任务已改变了优先级.
	 }

	 // ... 

	 // 我们的优先级比创建的优先级高吗? 译注(这里只是为了演示NULL句柄的使用)
	 if( uxTaskPriorityGet( xHandle ) < uxTaskPriorityGet( NULL ) )
	 {
		 // 我们的优先级 (使用NULL句柄获取)更高.
	 }
 }
 */
UBaseType_t uxTaskPriorityGet( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * UBaseType_t uxTaskPriorityGetFromISR( TaskHandle_t xTask );
 * 可以在ISR(中断服务)程序中获取优先级的版本
 */
UBaseType_t uxTaskPriorityGetFromISR( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>eTaskState eTaskGetState( TaskHandle_t xTask );</pre>
 *
 * INCLUDE_eTaskGetState 必须定义为1，该函数才可以用。具体请看配置
 *
 * 获取任意任务的状态. 所有可能的状态都在eTaskState 枚举类型中
 *
 * @param xTask 要查询任务的句柄.
 *
 * @return xTask的状态是调用该函数时的状态.
 * 注意：在该函数被调用后，到对该函数返回状态进行检查之间，
 *  状态可能发生了改变
 */
eTaskState eTaskGetState( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * void vTaskGetInfo( TaskHandle_t xTask, TaskStatus_t *pxTaskStatus, BaseType_t xGetFreeStackSpace, eTaskState eState );
 *
 * configUSE_TRACE_FACILITY 在FreeRTOSConfig.h定义为1,该函数才能使用
 *
 * 把任务的有关信息填充到TaskStatus_t类型结构中
 *
 * @param xTask 要查询任务的句柄. NULL表示获取调用任务的信息
 *
 * @param pxTaskStatus  一个指向TaskStatus_t结构类型的指针，
 * 它将会被指向任务引用的参数xTask中，与任务相关的信息填充。
 *
 * @xGetFreeStackSpace TaskStatus_t中有一个成员表示被查询
 * 任务栈的水位标记(即栈空间用了多少)信息的,
 * 计算栈的水位信息相对来说需花比较长的时间，并使系统临时无响应,
 * 所以xGetFreeStackSpace参数，用于表示是否要跳过对栈的水位信息的检测。
 * 只有xGetFreeStackSpace不为pdFALSE时，才去获取栈的水位信息
 *
 * @param eState TaskStatus_t中有一个成员表示被查询任务的状态的
 * 获取任务状态并不是简单的一个赋值语句-所以需要提供一个参数表示
 * 是否略过获取任务状态;若要获取任务状态，则eState要设为eInvalid-
 * 否则传入的eState的值将会填充到TaskStatus_t中去
 *
 * 使用示例:
   <pre>
 void vAFunction( void )
 {
 TaskHandle_t xHandle;
 TaskStatus_t xTaskDetails;

    // 通过名称获取任务句柄.
    xHandle = xTaskGetHandle( "Task_Name" );

    // 检查句柄是否为空.
    configASSERT( xHandle );

    // 使用句柄进一步获取任务信息
    vTaskGetInfo( xHandle,
                  &xTaskDetails,
                  pdTRUE, // 包括获取栈的水位信息
                  eInvalid ); // 包括获取任务状态详情
 }
 */
void vTaskGetInfo( TaskHandle_t xTask, TaskStatus_t *pxTaskStatus, BaseType_t xGetFreeStackSpace, eTaskState eState ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>void vTaskPrioritySet( TaskHandle_t xTask, UBaseType_t uxNewPriority );</pre>
 *
 * INCLUDE_vTaskPrioritySet 在FreeRTOSConfig.h定义为1,该函数才能使用
 *
 * 设置任意任务的优先级
 *
 * 如果设置的优先级比当前执行任务的优先级高，在函数返回之前将会产生上下文切换
 *
 * @param xTask 设置优先级的任务句柄.
 * NULL 表示设置调用任务的优先级.
 *
 * @param uxNewPriority 优先级的值.
 *
 * 使用示例:
 void vAFunction( void )
 {
 TaskHandle_t xHandle;

	 // 创建任务，存储句柄
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, &xHandle );

	 // ...

	 // 使用句柄提升任务的优先级
	 vTaskPrioritySet( xHandle, tskIDLE_PRIORITY + 1 );

	 // ...

	 // 使用NULL提升我们任务的句柄.
	 vTaskPrioritySet( NULL, tskIDLE_PRIORITY + 1 );
 }
 */
void vTaskPrioritySet( TaskHandle_t xTask, UBaseType_t uxNewPriority ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * void vTaskSuspend( TaskHandle_t xTaskToSuspend );
 *
 * INCLUDE_vTaskSuspend 在FreeRTOSConfig.h定义为1,该函数才能使用
 *
 * 挂起任何任务. 挂起的任务将不会得到 cpu 运行时间,而与其优先级无关
 *
 * vTaskSuspend的调用不会被累积 -
 * 比如:比如对同一任务调用两次vTaskSuspend，
 * 也仅需调用一次vTaskResume就可以让任务进入就绪态
 *
 * @param xTaskToSuspend 需要挂起任务的句柄，传递NULL,
 * 表示挂起调用任务
 *
 * 使用示例:
 void vAFunction( void )
 {
 TaskHandle_t xHandle;

	 // 创建任务，存储句柄
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, &xHandle );

	 // ...

	 // 使用句柄挂起任务.
	 vTaskSuspend( xHandle );

	 // ...

	 // 在此期间任务将不会运行，除非其他任务调用 vTaskResume( xHandle ).

	 //...


	 // 把自己挂起.
	 vTaskSuspend( NULL );

	 // 我们不会运行到这里，除非其他任务使用我们的句柄调用vTaskResume
 }
 */
void vTaskSuspend( TaskHandle_t xTaskToSuspend ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>void vTaskResume( TaskHandle_t xTaskToResume );</pre>
 *
 * INCLUDE_vTaskSuspend 在FreeRTOSConfig.h定义为1,该函数才能使用
 *
 * 恢得一个挂起任务
 * 某个任务被调用了一次或多次vTaskSuspend，但只要调用一次vTaskResume，
 * 它就可以再次运行
 *
 * @param xTaskToResume 需要恢复的任务句柄.
 *
 * 使用示例:
 void vAFunction( void )
 {
 TaskHandle_t xHandle;

	 // 创建任务，存储句柄
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, &xHandle );

	 // ...

	 // 使用句柄挂起创建的任务
	 vTaskSuspend( xHandle );

	 // ...

	 // 在此期间任务将不会运行，除非其他任务调用 vTaskResume( xHandle ).

	 //...


	 // 恢复挂起的任务
	 vTaskResume( xHandle );

	 // 被创建的任务根据它在系统中的优先级，将再次获得cpu运行时间.
 }
 */
void vTaskResume( TaskHandle_t xTaskToResume ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>void xTaskResumeFromISR( TaskHandle_t xTaskToResume );</pre>
 *
 * INCLUDE_xTaskResumeFromISR 在FreeRTOSConfig.h定义为1,该函数才能使用
 *
 * 可以在ISR 中调用的另一个vTaskResume实现版本
 *
 * 某个任务被调用了一次或多次vTaskSuspend，但只要调用一次 xTaskResumeFromISR，
 * 它就可以再次运行
 *
 * xTaskResumeFromISR() 该函数不应用于任务与中断的同步，因为中断有机率先于任务被挂起
 * 这将会导致任务有可能被错失. 使用semaphore机制，能避免这种问题
 *
 * @param xTaskToResume 需要恢复的任务句柄.
 *
 * @return 如果恢复该任务将导致上下文切换，返回pdTRUE，否则返回pdFALSE
 * 这是因为ISR需要使用该返回值来确定在ISR之后是否需要上下文切换。 
 */
BaseType_t xTaskResumeFromISR( TaskHandle_t xTaskToResume ) PRIVILEGED_FUNCTION;

/*-----------------------------------------------------------
 * 调度控制
 *----------------------------------------------------------*/

/**
 * task. h
 * void vTaskStartScheduler( void );
 *
 * 启动实时内核tick处理.  调用后，内核可以控制执行哪些任务以及何时执行。
 *
 * 请参考 main.c中，关于创建和任务和启动内核
 *
 * 使用示例:
 void vAFunction( void )
 {
	 // 在启动内核前至少创建一个任务.
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );

	 // 用抢占启动实时内核.
	 vTaskStartScheduler ();

	 // 不会运行到这里，除非任务调用了 vTaskEndScheduler ()
 }
 */
void vTaskStartScheduler( void ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>void vTaskEndScheduler( void );</pre>
 *
 * 注意:  在写这篇文章时，只有x86实模式端口（它在PC上运行，而不是DOS）实现了这个功能。
 *	
 * 停止内核时钟节拍，所有创建的任务将会自动释放，多任务(无论是抢占式或是协作式)将会停止
 * 然后执行将会恢复到vTaskStartScheduler调用处，就好像vTaskStartScheduler刚刚返回。
 *
 * 请查看 demo/PC目录一个示例应用的main.c中有调用vTaskEndScheduler ().
 *
 * vTaskEndScheduler () 需要在移植层提供一个退出函数
 * (请参照PCt移植中的 port.c文件中的 vPortEndScheduler).  This
 * 这将会执行与硬件相关的操作，比停止内核tick
 *
 * vTaskEndScheduler () 将会导致所有由内核分配的内存被释放--
 * 但不会释放应用任务分配的资源
 * 使用示例:
 void vTaskCode( void * pvParameters )
 {
	 for( ;; )
	 {
		 // 这里写任务代码.

		 // 在某个点我们想要结束实时内核处理
		 // 所以调用了
		 vTaskEndScheduler ();
	 }
 }

 void vAFunction( void )
 {
	 // 在启动内核前至少创建一个任务.
	 xTaskCreate( vTaskCode, "NAME", STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );

	 // 用抢占启动实时内核.
	 vTaskStartScheduler ();

	 // 只有任务调用vTaskEndScheduler，才会到达这里
	 // 当我们到达这里时，又恢到单任务执行模式
 }
 */
void vTaskEndScheduler( void ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>void vTaskSuspendAll( void );</pre>
 *
 * 在不禁用中断的情况下挂起调度程序。当调度程序挂起时，不会发生上下文切换。
 *
 * 调用vTaskSuspendAll（）之后，调用任务将继续执行
 * 在调用xTaskResumeAll（）之前没有被替换的风险。
 *
 * 可能导致上下文切换的API函数
 *（例如，vTaskDelayUntil（）、xQueueSend（）等）不能在调度程序挂起时调用。
 *
 * 使用示例:
 void vTask1( void * pvParameters )
 {
	 for( ;; )
	 {
		 // 任务函数写在这里.

		 // ...

		 // 在某个点需要执行一个占时很长的操作，并不希望被交换出去。
		 // 它不能用taskENTER_CRITICAL ()/taskEXIT_CRITICAL () 
		 // 因为操作占时很长，这样会导致中断丢失-包括ticks

		 // 阻止内核把任务交换出去
		 vTaskSuspendAll ();

		 // 执行某个操作，这时我们不需要用临界区，因为我们占用了所有的cpu处理时间
		 // 在这段时间内中断将继续进行，并且内核tick也会维持

		 // ...

		 // 操作结束.  重启内核.
		 xTaskResumeAll ();
	 }
 }
 */
void vTaskSuspendAll( void ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <pre>BaseType_t xTaskResumeAll( void );</pre>
 *
 * 恢得被vTaskSuspendAll()调用挂起的调度器
 * 
 * xTaskResumeAll()仅恢复调度器，而不会恢复之前由vTaskSuspend()挂起的任务
 *
 * @return 如果恢复度器会导到上下文切换那么会返回pdTRUE,
 * 否则返回pdFALSE
 *
 * 使用示例:
 void vTask1( void * pvParameters )
 {
	 for( ;; )
	 {
		 // 任务代码.

		 // ...

		 // 在某个点需要执行一个占时很长的操作，并不希望被交换出去。
		 // 它不能用taskENTER_CRITICAL ()/taskEXIT_CRITICAL () 
		 // 因为操作占时很长，这样会导致中断丢失-包括ticks


		 // 阻止内核把任务交换出去
		 vTaskSuspendAll ();

		 // 执行某个操作，这时我们不需要用临界区，因为我们占用了所有的cpu处理时间
		 // 在这段时间内中断将继续进行，并且内核tick也会维持

		 // ...

		 // 操作结束.  重启内核.  我们想强制进行上下文切换
		 // - 但是如果恢复调度器已导致了上下文切换，就不需再切换了。
		 if( !xTaskResumeAll () )
		 {
			  taskYIELD ();
		 }
	 }
 }
 */
BaseType_t xTaskResumeAll( void ) PRIVILEGED_FUNCTION;

/*-----------------------------------------------------------
 * 任务实用函数
 *----------------------------------------------------------*/

/**
 * task. h
 * TickType_t xTaskGetTickCount( void );
 * @return 获取从vTaskStartScheduler开始启动后的 tick 计数
 */
TickType_t xTaskGetTickCount( void ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * TickType_t xTaskGetTickCountFromISR( void );
 *
 * @return 获取从vTaskStartScheduler开始启动后的 tick 计数
 *
 * 这是xTaskGetTickCount（）的一个版本，可以从ISR安全调用，
 * 前提是TickType_t是所使用微控制器的自然字大小，
 * 或者中断嵌套不受支持或不被使用。
 */
TickType_t xTaskGetTickCountFromISR( void ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <PRE>uint16_t uxTaskGetNumberOfTasks( void );</PRE>
 *
 * @return 返回内核当前管理的任务数,包括就绪，阻塞与挂起的任务
 * 被删除但是还没有被空闲任务释放的任务也被计算在内
 */
UBaseType_t uxTaskGetNumberOfTasks( void ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * char *pcTaskGetName( TaskHandle_t xTaskToQuery );
 *
 * @return 返回任务的文本名称
 * 任务查询自己的名称可以通过句柄也可以通过NULL值
 */
char *pcTaskGetName( TaskHandle_t xTaskToQuery ) PRIVILEGED_FUNCTION; /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */

/**
 * task. h
 * TaskHandle_t xTaskGetHandle( const char *pcNameToQuery );
 * INCLUDE_xTaskGetHandle 在FreeRTOSConfig.h中必须设为1,此函数才可用
 * 注意:  完成此功能需要相对较长的时间，应谨慎使用。
 * 
 * @return 返回任务可读的名称.
 * 返回NULL，表示没有找到 
 */
TaskHandle_t xTaskGetHandle( const char *pcNameToQuery ) PRIVILEGED_FUNCTION; /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */

/**
 * task.h
 * UBaseType_t uxTaskGetStackHighWaterMark( TaskHandle_t xTask );
 *
 * INCLUDE_uxTaskGetStackHighWaterMark在FreeRTOSConfig.h 必须设为1,此函数才可用
 *
 * 返回指定任务的高水位标记.  也就是任务启动后，栈的可用空间(
 * 以字为单位，在32bit芯片中，1表示4字节).返回的值越小表示越接近栈溢出
 *
 * @param xTask 要检查栈的任务句柄.
 * 该参数为NULL，表示获取调用任务的可用栈空间
 *
 * @return 返回xTask所引用的任务,自创建后可用的栈空间 
 * (以字为单位，而不是以字节为单位)
 */
UBaseType_t uxTaskGetStackHighWaterMark( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;

/* 使用跟踪宏时，有时需要在FreeRTOS.h之前包含task.h。
这样会导致TaskHookFunction_t尚未定义，因此以下两个原型将导致编译错误。
这可以通过简单地防止包含这两个原型来解决，除非configUSE_APPLICATION_TASK_TAG配置常量明确要求它们
*/
#ifdef configUSE_APPLICATION_TASK_TAG
	#if configUSE_APPLICATION_TASK_TAG == 1
		/**
		 * task.h
		 * <pre>void vTaskSetApplicationTaskTag( TaskHandle_t xTask, TaskHookFunction_t pxHookFunction );</pre>
		 *
		 * 将pxHookFunction设置为任务xTask的任务钩子函数
		 * Sets pxHookFunction to be the task hook function used by the task xTask.
		 * xTask 为NULL 即为设置调用函数的钩子函数
		 */
		void vTaskSetApplicationTaskTag( TaskHandle_t xTask, TaskHookFunction_t pxHookFunction ) PRIVILEGED_FUNCTION;

		/**
		 * task.h
		 * <pre>void xTaskGetApplicationTaskTag( TaskHandle_t xTask );</pre>
		 *
		 * 返回设置给xTask的钩子函数pxHookFunction
		 */
		TaskHookFunction_t xTaskGetApplicationTaskTag( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;
	#endif /* configUSE_APPLICATION_TASK_TAG ==1 */
#endif /* ifdef configUSE_APPLICATION_TASK_TAG */

#if( configNUM_THREAD_LOCAL_STORAGE_POINTERS > 0 )

	/* 每个任务都包含一个指针数组，在FreeRTOSConfig.h中
	由configNUM_THREAD_LOCAL_STORAGE_POINTERS指定数组的大小。
	内核本身不使用这些指针，应用实现者可以用这些指针来做自己想做的事。
    下面这两个函数分别用于查询和获取指针 */
	void vTaskSetThreadLocalStoragePointer( TaskHandle_t xTaskToSet, BaseType_t xIndex, void *pvValue ) PRIVILEGED_FUNCTION;
	void *pvTaskGetThreadLocalStoragePointer( TaskHandle_t xTaskToQuery, BaseType_t xIndex ) PRIVILEGED_FUNCTION;

#endif

/**
 * task.h
 * BaseType_t xTaskCallApplicationTaskHook( TaskHandle_t xTask, void *pvParameter );
 *
 * 调用与xTask关联的钩子函数. 传NULL表示调用函数的任务，即调用该函数的任务的钩子函数
 *
 * pvParameter 是传给钩子函数的，以便任务按其需要解释
 * wants.返回值就是注册的钩子函数返回的值
 */
BaseType_t xTaskCallApplicationTaskHook( TaskHandle_t xTask, void *pvParameter ) PRIVILEGED_FUNCTION;

/**
 * INCLUDE_xTaskGetIdleTaskHandle 在FreeRTOSConfig.h为 1 ,xTaskGetIdleTaskHandle() 才可用.
 * 简单的返回空闲任务的句柄，在xTaskGetIdleTaskHandle启动之前，调用此函数是无效的
 */
TaskHandle_t xTaskGetIdleTaskHandle( void ) PRIVILEGED_FUNCTION;

/**
 * configUSE_TRACE_FACILITY 必须在FreeRTOSConfig.h定为 1
 * uxTaskGetSystemState() 函数才可用.
 *
 * uxTaskGetSystemState() 填充系统中每个任务的结构体
 * TaskStatus结构包含成员或任务句柄、任务名称、任务优先级、
 * 任务状态和任务消耗的运行时间总量。
 * 有关完整成员列表，请参阅此文件中的TaskStatus结构定义。
 *
 * 注意: 此函数仅用于调试用途，因为它的使用会导致调度程序长时间挂起
 *
 * @param pxTaskStatusArray 指向TaskStatus结构数组的指针.
 * 数组必须给RTOS控制的每一个任务，一个相应的结构体。 RTOS控制的任务数量
 * 可以由uxTaskGetNumberOfTasks()函数获得
 *
 * @param uxArraySize pxTaskStatusArray参数指向的数组的大小
 * 指数组索引的大小，或者数组中包含的结构数量，不是数组的字节数
 *
 * @param pulTotalRunTime 如果configGENERATE_RUN_TIME_STATS设置为1,那么
 * *pulTotalRunTime 将会被设置为uxTaskGetSystemState()获取的总运行时间值
 * (参考 http://www.freertos.org/rtos-run-time-stats.html)
 * pulTotalRunTime可以设为NULL,这样就可以不获取运行时间信息
 *
 * @return 返回uxTaskGetSystemState()填充TaskStatus_t的结构数量
 * 这个值应与uxTaskGetNumberOfTasks()获取的数量一样,
 * 但如果uxArraySize的值太小，那么将返回0
 *
 * 使用示例:
 	// 这个示例演示了把uxTaskGetSystemState()函数获取的原始数据，
 	// 如何转换成我们认识的表格信息.
    // 可读取的表格数据写在pcWriteBuffer中
	void vTaskGetRunTimeStats( char *pcWriteBuffer )
	{
	TaskStatus_t *pxTaskStatusArray;
	volatile UBaseType_t uxArraySize, x;
	uint32_t ulTotalRunTime, ulStatsAsPercentage;

		// 确保内存不包括字符串.
		*pcWriteBuffer = 0x00;

		// 获取任务数量，以防在执行此函数时发生更改。
		uxArraySize = uxTaskGetNumberOfTasks();

		// 为每个任务分配一个TaskStatus_t结构体, 也可以编译器静态的分配
		pxTaskStatusArray = pvPortMalloc( uxArraySize * sizeof( TaskStatus_t ) );

		if( pxTaskStatusArray != NULL )
		{
			// 获取每个任务的原始数据.
			uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, &ulTotalRunTime );

			// 计算百分比.
			ulTotalRunTime /= 100UL;

			// 辟免除0错误.
			if( ulTotalRunTime > 0 )
			{
				// 遍历数组中每一个结构体,将其格式化为可读的ASCII数据
				for( x = 0; x < uxArraySize; x++ )
				{
					// 任务运行的时间占总运行时间的百分比是多少?
					// 向下舍入到最接近的整数
					// ulTotalRunTime 这个值已经除以了 100.
					ulStatsAsPercentage = pxTaskStatusArray[ x ].ulRunTimeCounter / ulTotalRunTime;

					if( ulStatsAsPercentage > 0UL )
					{
						sprintf( pcWriteBuffer, "%s\t\t%lu\t\t%lu%%\r\n", pxTaskStatusArray[ x ].pcTaskName, pxTaskStatusArray[ x ].ulRunTimeCounter, ulStatsAsPercentage );
					}
					else
					{
						// 如果百分比是0,说明该任务运行时间占总运行时间的百分比少于1%
						sprintf( pcWriteBuffer, "%s\t\t%lu\t\t<1%%\r\n", pxTaskStatusArray[ x ].pcTaskName, pxTaskStatusArray[ x ].ulRunTimeCounter );
					}

					pcWriteBuffer += strlen( ( char * ) pcWriteBuffer );
				}
			}

			// 数组不再需要，释放它所占的内存
			vPortFree( pxTaskStatusArray );
		}
	}
 */
UBaseType_t uxTaskGetSystemState( TaskStatus_t * const pxTaskStatusArray, const UBaseType_t uxArraySize, uint32_t * const pulTotalRunTime ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * void vTaskList( char *pcWriteBuffer );
 * 要使该函数可用，configUSE_TRACE_FACILITY和configUSE_STATS_FORMATTING_FUNCTIONS
 * 都必须定义为1.请到FreeRTOS.org网站了解详细信息
 *
 * 注意: 该函数运行时要禁止中断，该函数仅用于协助调试。
 *
 * 列出所有当前任务及其当前状态和堆栈使用率高水位线。
 * 任务报告表中 ('B')表示阻塞,('R')表示就绪，('D')表示删除，('S')表示挂起
 *
 * 请注意:
 *	
 * 此函数仅为方便使用而提供，许多演示应用程序都使用此函数。
 * 不要认为它是调度程序的一部分。
 * 
 * 该函数调用uxTaskGetSystemState(),然后把获取的值转换成方便我们可读的显示表格
 * 包括任务名，状态和栈的使用情况
 *
 * vTaskList() 还依赖一个标准C库函数，可能会导致代码膨胀，使用大量的栈，
 * 并且不同平台结果不一样。在很多FreeRTOS/Demo的子目录中有一个printf-stdarg.c，
 * 提供了sprintf函数的替代函数.(注意printf-stdarg.c中没有snprintf()函数的完全实现)
 *
 * 推荐在生产系统中直接调用uxTaskGetSystemState()获取原始数据，
 * 而不要通过vTaskList()间接的调用uxTaskGetSystemState
 *
 * @param pcWriteBuffer 这个buffer就是上面提到的，用来写入ASCII格式的详细信息
 * 这里假设这个buffer足够大，能够写入所有产生的信息
 * 每个任务约40字节就够了
 */
void vTaskList( char * pcWriteBuffer ) PRIVILEGED_FUNCTION; /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */

/**
 * task. h
 * void vTaskGetRunTimeStats( char *pcWriteBuffer );
 * 
 * configGENERATE_RUN_TIME_STATS 和 configUSE_STATS_FORMATTING_FUNCTIONS
 * 必须设置为1,并且提供如下宏的定义
 * portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() 外设定时器
 * portGET_RUN_TIME_COUNTER_VALUE()	和计数值 返回计数时间值
 * 计数器应至少是滴答计数频率的10倍。
 *
 * 注意1: 该函数运行时要禁止中断，该函数仅用于协助调试。
 *
 * Setting configGENERATE_RUN_TIME_STATS to 1 will result in a total
 * accumulated execution time being stored for each task.  The resolution
 * of the accumulated time value depends on the frequency of the timer
 * configured by the portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() macro.
 * Calling vTaskGetRunTimeStats() writes the total execution time of each
 * task into a buffer, both as an absolute count value and as a percentage
 * of the total system execution time.
 *
 * 注意2: 此函数仅为方便使用而提供，许多演示应用程序都使用此函数。
 * 不要认为它是调度程序的一部分。
 * 
 * vTaskGetRunTimeStats()调用uxTaskGetSystemState(),然后格式化了uxTaskGetSystemState()
 * 的部分输出，以绝对值和百分比的方式显示了每个任务的运行时间量
 *
 * vTaskGetRunTimeStats() 还依赖一个标准C库函数，可能会导致代码膨胀，使用大量的栈，
 * 并且不同平台结果不一样。在很多FreeRTOS/Demo的子目录中有一个printf-stdarg.c，
 * 提供了sprintf函数的替代函数.(注意printf-stdarg.c中没有snprintf()函数的完全实现)
 *
 * 推荐在生产系统中直接调用uxTaskGetSystemState()获取原始数据，
 * 而不要通过vTaskGetRunTimeStats()间接的调用uxTaskGetSystemState
 *
 * @param pcWriteBuffer 这个buffer就是上面提到的，用来写入ASCII格式的详细信息
 * 这里假设这个buffer足够大，能够写入所有产生的信息
 * 每个任务约40字节就够了
 */
void vTaskGetRunTimeStats( char *pcWriteBuffer ) PRIVILEGED_FUNCTION; /*lint !e971 只允许字符串和单个字符使用非限定字符类型. */

/**
 * task. h
 * BaseType_t xTaskNotify( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction );
 *
 * 要使该函数可用 configUSE_TASK_NOTIFICATIONS 必须为1 
 * 当 configUSE_TASK_NOTIFICATIONS 置为1，每个任务将有一个无符号32-bit整型的私有成员
 *
 * 发送给任务的事件都使用了一个中间对象，比如队列，信号量，互斥量和事件组。
 * 而任务通知是是直接将事件直接发送给任务，而不使用中间对象的一种事件通知方式。
 *	
 * 发送到任务的通知可以选择性地执行操作，例如更新、覆盖或增加任务的通知值。
 * 通过这种方式，任务通知可以用于向任务发送数据，
 * 或者用轻量级和快速的二进制或计数信号量。
 *
 * 发送到任务的通知将保持挂起状态，直到调用xTaskNotifyWait（）
 * 或ulTaskNotifyTake（）的任务将其清除。如果任务已经处于挂起状态，并等待通知，
 * 那么当通知到来时，将会自动解除阻塞状态并清除通知标志位
 *
 * 任务可以使用xTaskNotifyWait（）to[可选]block等待通知挂起，
 * 或使用ulTaskNotifyTake（）到[可选]block等待其通知值具有非零值。
 * 任务处于阻塞状态时不会占用任何CPU时间。
 *
 * 参考网址获取详细信息： http://www.FreeRTOS.org/RTOS-task-notifications.html .
 *
 * @param xTaskToNotify 被通知任务的句柄. 任务句柄可以通过xTaskCreate()返回值得到
 * 并且还可以在当前任务中通过xTaskGetCurrentTaskHandle()函数得到
 *
 * @param ulValue 该数据可以与通知一起发给任务，
 * 具体如何使用该数据,取决于的eAction参数值
 *
 * @param eAction 指定了如何更新任务通知值(如果有值的话)
 * 每个操作的有效值如下:
 *
 * eSetBits -
 * 任务的通知值与ulValue按位或。在这种情况下，xTaskNofify（）始终返回pdPASS。
 *
 * eIncrement -
 * 任务的通知值将递增。在这种情况下，不使用ulValue，xTaskNotify（）始终返回pdPASS。
 *
 * eSetValueWithOverwrite -
 * 任务的通知值设置为ulValue的值，即使被通知的任务尚未处理上一个通知
 * （该任务已经有一个通知挂起）。在这种情况下，xTaskNotify（）始终返回pdPASS。
 *
 * eSetValueWithoutOverwrite -
 * 如果被通知的任务没有挂起的通知，则任务的通知值设置为ulValue，
 * xTaskNotify（）将返回pdPASS。如果被通知的任务已经收到通知挂起，
 * 则不执行任何操作并返回pdFAIL。
 
 * eNoAction -
 * 任务接收通知，但其通知值未更新。在这种情况下，
 * 不使用ulValue，xTaskNotify（）始终返回pdPASS。
 *
 *  pulPreviousNotificationValue -
 *  用于输出被通知函数修改之前的任务通知值
 *
 * @return 取决于eAction值. 参考前面的描述
 *
 */
BaseType_t xTaskGenericNotify( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction, uint32_t *pulPreviousNotificationValue ) PRIVILEGED_FUNCTION;
#define xTaskNotify( xTaskToNotify, ulValue, eAction ) xTaskGenericNotify( ( xTaskToNotify ), ( ulValue ), ( eAction ), NULL )
#define xTaskNotifyAndQuery( xTaskToNotify, ulValue, eAction, pulPreviousNotifyValue ) xTaskGenericNotify( ( xTaskToNotify ), ( ulValue ), ( eAction ), ( pulPreviousNotifyValue ) )

/**
 * task. h
 * BaseType_t xTaskNotifyFromISR( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction, BaseType_t *pxHigherPriorityTaskWoken );</PRE>
 *
 * configUSE_TASK_NOTIFICATIONS 没有定义或者被定义为1，该函数才可用
 *
 * 当 configUSE_TASK_NOTIFICATIONS 置为1，每个任务将有一个无符号32-bit整型的私有成员
 *
 * xTaskNotify()函数的中断服务程序版本
 *
 * 发送给任务的事件都使用了一个中间对象，比如队列，信号量，互斥量和事件组。
 * 而任务通知是是直接将事件直接发送给任务，而不使用中间对象的一种事件通知方式。
 *
 * 发送到任务的通知可以选择性地执行操作，例如更新、覆盖或增加任务的通知值。
 * 通过这种方式，任务通知可以用于向任务发送数据，
 * 或者用轻量级和快速的二进制或计数信号量。
 *
 * 发送到任务的通知将保持挂起状态，直到调用xTaskNotifyWait（）
 * 或ulTaskNotifyTake（）的任务将其清除。如果任务已经处于挂起状态，并等待通知，
 * 那么当通知到来时，将会自动解除阻塞状态并清除通知标志位
 *
 * 任务可以使用xTaskNotifyWait（）to[可选]block等待通知挂起，
 * 或使用ulTaskNotifyTake（）到[可选]block等待其通知值具有非零值。
 * 任务处于阻塞状态时不会占用任何CPU时间。
 *
 * 参考网址获取详细信息： http://www.FreeRTOS.org/RTOS-task-notifications.html .
 *
 * @param xTaskToNotify 被通知任务的句柄. 任务句柄可以通过xTaskCreate()返回值得到
 * 并且还可以在当前任务中通过xTaskGetCurrentTaskHandle()函数得到
 *
 * @param ulValue 该数据可以与通知一起发给任务，
 * 具体如何使用该数据,取决于的eAction参数值
 *
 * @param eAction 指定了如何更新任务通知值(如果有值的话)
 * 每个操作的有效值如下:
 *
 * eSetBits -
 * 任务的通知值与ulValue按位或。在这种情况下，xTaskNofify（）始终返回pdPASS。
 *
 * eIncrement -
 * 任务的通知值将递增。在这种情况下，不使用ulValue，xTaskNotify（）始终返回pdPASS。
 *
 * eSetValueWithOverwrite -
 * 任务的通知值设置为ulValue的值，即使被通知的任务尚未处理上一个通知
 * （该任务已经有一个通知挂起）。在这种情况下，xTaskNotify（）始终返回pdPASS。
 *
 * eSetValueWithoutOverwrite -
 * 如果被通知的任务没有挂起的通知，则任务的通知值设置为ulValue，
 * xTaskNotify（）将返回pdPASS。如果被通知的任务已经收到通知挂起，
 * 则不执行任何操作并返回pdFAIL。
 *
 * eNoAction -
 * 任务接收通知，但其通知值未更新。在这种情况下，
 * 不使用ulValue，xTaskNotify（）始终返回pdPASS。
 *
 * @param pxHigherPriorityTaskWoken 如果发送的通知,导致接收通知的 
 * 任务离开阻塞状态，并且离开阻塞的任务的优先级比发送通知的任务的优先
 * 级高，则xTaskNotifyFromISR()会将*pxHigherPriorityTaskWoken置为pdTRUE. 
 * 如果xTaskNotifyFromISR()把*pxHigherPriorityTaskWoken置为pdTRUE,那么在
 * 退出中断之前，上下文切换将会被请求。在ISR中如何进行上下文切换的请求，取决
 * 移植实现--具体请查看移植相关文档
 *
 * @return 取决于eAction值. 参考前面的描述
 *
 */
BaseType_t xTaskGenericNotifyFromISR( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction, uint32_t *pulPreviousNotificationValue, BaseType_t *pxHigherPriorityTaskWoken ) PRIVILEGED_FUNCTION;
#define xTaskNotifyFromISR( xTaskToNotify, ulValue, eAction, pxHigherPriorityTaskWoken ) xTaskGenericNotifyFromISR( ( xTaskToNotify ), ( ulValue ), ( eAction ), NULL, ( pxHigherPriorityTaskWoken ) )
#define xTaskNotifyAndQueryFromISR( xTaskToNotify, ulValue, eAction, pulPreviousNotificationValue, pxHigherPriorityTaskWoken ) xTaskGenericNotifyFromISR( ( xTaskToNotify ), ( ulValue ), ( eAction ), ( pulPreviousNotificationValue ), ( pxHigherPriorityTaskWoken ) )

/**
 * task. h
 * BaseType_t xTaskNotifyWait( uint32_t ulBitsToClearOnEntry, uint32_t ulBitsToClearOnExit, uint32_t *pulNotificationValue, TickType_t xTicksToWait );
 *
 * configUSE_TASK_NOTIFICATIONS 被定义为1，该函数才可用
 *
 * 当 configUSE_TASK_NOTIFICATIONS 置为1，每个任务将有一个无符号32-bit整型的私有成员
 *
 * 发送给任务的事件都使用了一个中间对象，比如队列，信号量，互斥量和事件组。
 * 而任务通知是是直接将事件直接发送给任务，而不使用中间对象的一种事件通知方式。
 *
 * 发送到任务的通知可以选择性地执行操作，例如更新、覆盖或增加任务的通知值。
 * 通过这种方式，任务通知可以用于向任务发送数据，
 * 或者用轻量级和快速的二进制或计数信号量。
 *
 * 发送到任务的通知将保持挂起状态，直到调用xTaskNotifyWait（）
 * 或ulTaskNotifyTake（）的任务将其清除。如果任务已经处于挂起状态，并等待通知，
 * 那么当通知到来时，将会自动解除阻塞状态并清除通知标志位
 *
 * 任务可以使用xTaskNotifyWait（）to[可选]block等待通知挂起，
 * 或使用ulTaskNotifyTake（）到[可选]block等待其通知值具有非零值。
 * 任务处于阻塞状态时不会占用任何CPU时间。
 *
 * 参考网址获取详细信息： http://www.FreeRTOS.org/RTOS-task-notifications.html .
 *
 * @param ulBitsToClearOnEntry 在任务检查是否有消息正在挂起，及如果没有消息正在挂起
 * 并阻塞它之前，ulBitsToClearOnEntry中的设置位将清除调用任务的通知值。
 * ulBitsToClearOnEntry为ULONG_MAX(如果包含了limits.h)或为0xffffffffUL，将能把
 * 任务的通知值清零；为0时不改变任务的通知值。
 *
 * @param ulBitsToClearOnExit 如果任务正在挂起或者接收到通知，并在调用任务退出
 * xTaskNotifyWait()函数之前，将通知值(参考xTaskNotify()函数)通过pulNotificationValue
 * 参数输出. 然后ulBitsToClearOnExit中设置的位将会在该函数退出之前清除任务通知值。
 * 注意:ulBitsToClearOnExit为ULONG_MAX(如果包含了limits.h)或为0xffffffffUL，将能把
 * 任务的通知值清零；为0时不改变任务的通知值(这种情况下pulNotificationValue的值与
 * 任务的通知值是一样的)。
 *
 * @param pulNotificationValue 用于把任务的通知值输出给函数
 * 注意：由于ulbitstoClearOneExit为非零而导致的任何位的清除不会影响该值的输出。
 *
 * @param xTicksToWait 任务在阻塞状态等待通知值的最大时间，当调用xTaskNotifyWait()函数
 * 时任务应该不正处于挂起状态。当任务处理阻塞态时，不会消耗任何cpu时间
 * 这个值是以tick为单位的，可以通过宏pdMS_TO_TICSK(value_in_ms)把毫秒值转换成tick
 *
 * @return 如果收到了一个通知值(包括调用xTaskNotifyWait时，正在挂起的通知值)，
 * 则返回pdPASS,否则返回pdFAIL 
 */
BaseType_t xTaskNotifyWait( uint32_t ulBitsToClearOnEntry, uint32_t ulBitsToClearOnExit, uint32_t *pulNotificationValue, TickType_t xTicksToWait ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * BaseType_t xTaskNotifyGive( TaskHandle_t xTaskToNotify );
 *  
 * configUSE_TASK_NOTIFICATIONS 没有定义或定义为1，该宏才可用
 *
 * 当 configUSE_TASK_NOTIFICATIONS 置为1，每个任务将有一个无符号32-bit整型的私有成员
 *
 * 发送给任务的事件都使用了一个中间对象，比如队列，信号量，互斥量和事件组。
 * 而任务通知是是直接将事件直接发送给任务，而不使用中间对象的一种事件通知方式。
 *
 * 发送到任务的通知可以选择性地执行操作，例如更新、覆盖或增加任务的通知值。
 * 通过这种方式，任务通知可以用于向任务发送数据，
 * 或者用轻量级和快速的二进制或计数信号量。
 *
 * xTaskNotifyGive（）是一个助手宏，用于任务通知时使用
 * 被用作轻量级和更快的二进制或计数信号量的等价物。
 *
 * 实际的FreeRTOS信号量是使用xSemaphoreGive（）API函数给出的，
 * 而使用任务通知的等效操作是xTaskNotifyGive（）。
 *
 * 当任务通知当用二值或计数信号量来调用时，那么任务应该用ulTaskNotificationTake()
 * 来等待信号量而不是用xTaskNotifyWait()
 * 参考 http://www.FreeRTOS.org/RTOS-task-notifications.html 获取详细.
 *
 * @param xTaskToNotify 被通知任务的句柄.  任务句柄可以通过xTaskCreate()返回值得到
 * 并且还可以在当前任务中通过xTaskGetCurrentTaskHandle()函数得到
 *
 * @return xTaskNotifyGive() 这是一个宏，调用的是xTaskNotify()，且eAction参数是eIncrement
 * 所以总是返回pdPASS
 */
#define xTaskNotifyGive( xTaskToNotify ) xTaskGenericNotify( ( xTaskToNotify ), ( 0 ), eIncrement, NULL )

/**
 * task. h
 * void vTaskNotifyGiveFromISR( TaskHandle_t xTaskHandle, BaseType_t *pxHigherPriorityTaskWoken );
 *
 * configUSE_TASK_NOTIFICATIONS 没有定义或定义为1，该宏才可用
 *
 * 当 configUSE_TASK_NOTIFICATIONS 置为1，每个任务将有一个无符号32-bit整型的私有成员
 *
 * xTaskNotifyGive()的(ISR)中断服务程序中调用版本
 *
 * 发送给任务的事件都使用了一个中间对象，比如队列，信号量，互斥量和事件组。
 * 而任务通知是是直接将事件直接发送给任务，而不使用中间对象的一种事件通知方式。
 *
 * 发送到任务的通知可以选择性地执行操作，例如更新、覆盖或增加任务的通知值。
 * 通过这种方式，任务通知可以用于向任务发送数据，
 * 或者用轻量级和快速的二进制或计数信号量。
 * 
 * 当将任务通知当作轻量级的二值信或计数信号量使用时，就可用vTaskNotifyGiveFromISR().
 * FreeRTOS 信号量在ISR中给出是用的xSemaphoreGiveFromISR()，任务通知给出函数
 * 是vTaskNotifyGiveFromISR()
 *
 * 当任务通知当用二值或计数信号量来调用时，那么任务应该用ulTaskNotificationTake()
 * 来等待信号量而不是用xTaskNotifyWait()
 * 参考 http://www.FreeRTOS.org/RTOS-task-notifications.html 获取详细.
 *
 * @param xTaskToNotify 被通知任务的句柄.  任务句柄可以通过xTaskCreate()返回值得到
 * 并且还可以在当前任务中通过xTaskGetCurrentTaskHandle()函数得到
 *
 * @param pxHigherPriorityTaskWoken  如果发送的通知,导致接收通知的 
 * 任务离开阻塞状态，并且离开阻塞的任务的优先级比发送通知的任务的优先
 * 级高，则xTaskNotifyFromISR()会将*pxHigherPriorityTaskWoken置为pdTRUE. 
 * 如果xTaskNotifyFromISR()把*pxHigherPriorityTaskWoken置为pdTRUE,那么在
 * 退出中断之前，上下文切换将会被请求。在ISR中如何进行上下文切换的请求，取决
 * 移植实现--具体请查看移植相关文档
 */
void vTaskNotifyGiveFromISR( TaskHandle_t xTaskToNotify, BaseType_t *pxHigherPriorityTaskWoken ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * uint32_t ulTaskNotifyTake( BaseType_t xClearCountOnExit, TickType_t xTicksToWait );
 *
 * configUSE_TASK_NOTIFICATIONS 没有定义或定义为1，该函数才可用
 *
 * 当 configUSE_TASK_NOTIFICATIONS 置为1，每个任务将有一个无符号32-bit整型的私有成员
 *
 * 发送给任务的事件都使用了一个中间对象，比如队列，信号量，互斥量和事件组。
 * 而任务通知是是直接将事件直接发送给任务，而不使用中间对象的一种事件通知方式。
 *
 * 发送到任务的通知可以选择性地执行操作，例如更新、覆盖或增加任务的通知值。
 * 通过这种方式，任务通知可以用于向任务发送数据，
 * 或者用轻量级和快速的二进制或计数信号量。 
 *
 * 当任务通知用作轻量级的二值与计数信号量的用时，就用ulTaskNotifyTake()
 * xSemaphoreTake()是信号量使用的API函数，而通知就使用ulTaskNotifyTake()
 * 
 * 当某个任务会将其通知值当用二值呀计数信号量使用时，其他的任务应该使用
 * xTaskNotifyGive()宏或者xTaskNotify() 函数并将其参数eAction设为eIncrement值
 * 
 * ulTaskNotifyTake()可以在退出时将通知值清为0，这种情况下通知值就像二值信号量
 * 或者在退出时，将通知值减少，这种情况下通知值就像计数信号量。
 *
 * 任务可以用ulTaskNotifyTake()来阻塞等待一个任务的非零通知值。处理阻塞态的任务
 * 不会消耗任何CPU时间
 *
 * 当任务的通知值挂起时，xTaskNotifyWait()函数返回
 * 当任务的通知值非零时，ulTaskNotifyTake()函数将返回
 * 参考 http://www.FreeRTOS.org/RTOS-task-notifications.html 获取详细.
 *
 * @param xClearCountOnExit 如果xClearCountOnExit为pdFALSE，
 * 那么在函数退出时通知值会减少,在这种情况下通知值就像一个计数信号量
 * 如果xClearCountOnExit值不为pdFALSE，那么在函数退出时，通知值将被清0
 * 在这种情况下，通知值就像一个二值信号量
 *
 * @param xTicksToWait 任务在阻塞状态等待通知值的最大时间，当调用xTaskNotifyWait()函数
 * 时任务应该不正处于挂起状态。当任务处理阻塞态时，不会消耗任何cpu时间
 * 这个值是以tick为单位的，可以通过宏pdMS_TO_TICSK(value_in_ms)把毫秒值转换成tick
 *
 * @return 任务清除为零或递减之前的通知计数（请参阅xClearCountOneExit参数）。
 *
 */
uint32_t ulTaskNotifyTake( BaseType_t xClearCountOnExit, TickType_t xTicksToWait ) PRIVILEGED_FUNCTION;

/**
 * task. h
 * <PRE>BaseType_t xTaskNotifyStateClear( TaskHandle_t xTask );</pre>
 *
 * 如果句柄xTask引用的任务的通知状态为eNotified，
 * 则将任务的通知状态设置为eNotWaitingNotification。
 * 任务的通知值不会更改。将xTask设置为NULL以清除调用任务的通知状态。
 *
 * @return  如果任务通知状态设为了eNotWaitingNotification，返回pdTRUE
 * 否则返回pdFALSE
 */
BaseType_t xTaskNotifyStateClear( TaskHandle_t xTask );

/*-----------------------------------------------------------
 * 只可用于移植的调度程序内部
 *----------------------------------------------------------*/

/*
 * 此函数不能从应用程序代码中使用。只是用于实现调度程序的端口时使用，
 * 并且调度程序专用的接口。
 * 
 * Called from the real time kernel tick (either preemptive or cooperative),
 * this increments the tick count and checks if any tasks that are blocked
 * for a finite period required removing from a blocked list and placing on
 * a ready list.  If a non-zero value is returned then a context switch is
 * required because either:
 *   + A task was removed from a blocked list because its timeout had expired,
 *     or
 *   + Time slicing is in use and there is a task of equal priority to the
 *     currently running task.
 */
BaseType_t xTaskIncrementTick( void ) PRIVILEGED_FUNCTION;

/*
 * 此函数不能从应用程序代码中使用。它只在实现调度器的端口时使用，
 * 是一个专用于调度器的接口。
 *
 * 调用此函数时必须禁用中断.
 *
 * 从就绪列表中移除调用任务，并将其同时放置在等待特定事件的任务列表和延迟的任务列表中。
 * 如果事件发生（并且没有更高优先级的任务等待同一事件）或延迟时间到期，
 * 任务将从两个列表中删除并替换到就绪列表中。
 * 
 * “无序”版本用xItemValue值替换事件列表项值，并在列表末尾插入列表项。
 * 
 * The 'ordered' version uses the existing event list item value (which is the
 * owning tasks priority) to insert the list item into the event list is task
 * priority order.
 *
 * @param pxEventList 这个列表包含的任务处于阻塞状态等待事件发生
 *
 * @param xItemValue 当事件列给不是以任务优先级排序时，这个值用于事件列表项
 *
 * @param xTicksToWait 任务等待事件发生的最大时件
 * 这个值是以内核时钟节拍tick为单位的，常量宏portTICK_PERIOD_MS
 * 可以把内核节拍转换成真实时间周期
 */
void vTaskPlaceOnEventList( List_t * const pxEventList, const TickType_t xTicksToWait ) PRIVILEGED_FUNCTION;
void vTaskPlaceOnUnorderedEventList( List_t * pxEventList, const TickType_t xItemValue, const TickType_t xTicksToWait ) PRIVILEGED_FUNCTION;

/*
 * 此函数不能从应用程序代码中使用。它只在实现调度器的端口时使用，
 * 是一个专用于调度器的接口。
 *
 * 调用此函数时必须禁用中断.
 *
 * 此函数与vTaskPlaceOnEventList()函数功能几乎一致，它们之间不同的时
 * 此函数不允许任务无限阻塞，而vTaskPlaceOnEventList()允许
 *
 */
void vTaskPlaceOnEventListRestricted( List_t * const pxEventList, TickType_t xTicksToWait, const BaseType_t xWaitIndefinitely ) PRIVILEGED_FUNCTION;

/*
 * 此函数不能从应用程序代码中使用。它只在实现调度器的端口时使用，是一个专用于调度器的接口。
 *
 * 调用此函数时必须禁用中断。
 *
 * 从指定的事件列表和阻止的列表中删除任务任务，并将其放置在就绪队列中。
 *
 * will be called
 * 如果一个解除阻塞的事件发生或者阻塞时间到期，函数xTaskRemoveFromEventList()
 * 或xTaskRemoveFromUnorderedEventList() 将会被调用
 *
 *	xTaskRemoveFromEventList()用于按优先级排序的事件列表中。它将从事件列表头移
 * 除列表项，因为事件列表中所有任务中它是捅有优先级最高的任务。
 * xTaskRemoveFromUnorderedEventList()当事件列表不是按优先级排序的列表
 * 并且事件列表持有的不是任务优先级。在这种情况下事件列表项值更新为参数xItemValue的值 
 *
 * @return 返回pdTRUE，如果移除的任务的优先级比调用任务的优先级高，否则返回pdFALSE
 */
BaseType_t xTaskRemoveFromEventList( const List_t * const pxEventList ) PRIVILEGED_FUNCTION;
BaseType_t xTaskRemoveFromUnorderedEventList( ListItem_t * pxEventListItem, const TickType_t xItemValue ) PRIVILEGED_FUNCTION;

/*
 * 此函数不能从应用程序代码中使用。它只在实现调度器的端口时使用，是一个专用于调度器的接口。
 * 将指向当前TCB的指针设置为准备运行的最高优先级任务的TCB。
 */
void vTaskSwitchContext( void ) PRIVILEGED_FUNCTION;

/*
 * 这些函数不能从应用程序代码中使用。它们由事件位模块使用。
 */
TickType_t uxTaskResetEventItemValue( void ) PRIVILEGED_FUNCTION;

/*
 * 返回调用任务的句柄。
 */
TaskHandle_t xTaskGetCurrentTaskHandle( void ) PRIVILEGED_FUNCTION;

/*
 * 捕获当前时间状态以供将来参考。
 */
void vTaskSetTimeOutState( TimeOut_t * const pxTimeOut ) PRIVILEGED_FUNCTION;

/*
 * 将现在的时间状态与以前捕获的时间状态进行比较，以查看超时是否已过期
 */
BaseType_t xTaskCheckForTimeOut( TimeOut_t * const pxTimeOut, TickType_t * const pxTicksToWait ) PRIVILEGED_FUNCTION;

/*
 * 队列实现用于防止对taskyield（）的不必要调用的快捷方式；
 */
void vTaskMissedYield( void ) PRIVILEGED_FUNCTION;

/*
 * 返回调度器的状态： taskSCHEDULER_RUNNING,
 * taskSCHEDULER_NOT_STARTED 或 taskSCHEDULER_SUSPENDED.
 */
BaseType_t xTaskGetSchedulerState( void ) PRIVILEGED_FUNCTION;

/*
 * 如果互斥锁持有者的优先级低于调用任务，则将互斥锁持有者的优先级提升到调用任务的优先级。
 */
void vTaskPriorityInherit( TaskHandle_t const pxMutexHolder ) PRIVILEGED_FUNCTION;

/*
 *如果任务在保持信号量时继承了更高的优先级，则将任务的优先级设置回正确的优先级。
 */
BaseType_t xTaskPriorityDisinherit( TaskHandle_t const pxMutexHolder ) PRIVILEGED_FUNCTION;

/*
 * 获取xTaskGet 参数指向的任务的任务编号.
 */
UBaseType_t uxTaskGetTaskNumber( TaskHandle_t xTask ) PRIVILEGED_FUNCTION;

/*
 * 设置xTask参数指向的任务的任务编号
 */
void vTaskSetTaskNumber( TaskHandle_t xTask, const UBaseType_t uxHandle ) PRIVILEGED_FUNCTION;

/*
 * 只有当configUSE_TICKLESS_IDLE 为1，才可用.
 * 如果正在使用tickless模式，或者实现了低功耗模式，则在空闲期间不会执行tick中断。
 * 在这种情况下，调度程序维护的tick count值需要通过向前跳过一个等于空闲时间段的
 * 时间来保持与实际执行时间最新。
 */
void vTaskStepTick( const TickType_t xTicksToJump ) PRIVILEGED_FUNCTION;

/*
 * 仅当configUSE_TICKLESS_IDLE为1时可用
 * 当使用portSUPPRESS_TICKS_AND_SLEEP()时，允许移植特定的sleep函数
 * 以决定是否继续sleep,是否继续运行，是否无限睡眠。
 *
 * 这个函数是必须的，因为portSUPPRESS_TICKS_AND_SLEEP()只有在调度起挂起时才会调用
 * 而不是临界区。因此在portSUPPRESS_TICKS_AND_SLEEP与真正进入低功耗模之间，是否可能
 * 发生中断请求的。该函数eTaskConfirmSleepModeStatus()应该在定时器被停止与进入睡眠
 * 模式之间，这一短小的关键时间之间被调用，以确保正确进入睡眠模式
 */
eSleepModeStatus eTaskConfirmSleepModeStatus( void ) PRIVILEGED_FUNCTION;

/*
 * 仅供内部使用.当一个mutex被持有时，增加它的计数.
 * 并返回持有这个互斥量的任务句柄
 */
void *pvTaskIncrementMutexHeldCount( void ) PRIVILEGED_FUNCTION;

#ifdef __cplusplus
}
#endif
#endif /* INC_TASK_H */



